from setuptools import setup, find_packages

with open('README.rst', 'rb') as f:
    long_descr = f.read().decode('utf-8')

setup(name='GV_tools',
    packages=find_packages('.', exclude=['test', 'tests']),
    package_dir={'':'.'},
    description = 'Tools for working with ground validation observations.',
    version='0.1',
    long_description=long_descr,
    author = 'Norm Wood',
    author_email = 'normw013@fastmail.fm',
    url = 'https://lecuyer.aos.wisc.edu/gitlab/norm/GV_tools',
    license = 'BSD-3-Clause',
    zip_safe = False,
    include_package_data = True,
   )
