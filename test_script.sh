#!/bin/sh

#Run the test() routines in the loader modules
python -m GV_tools.gcpex.loaders.dfir
python -m GV_tools.gcpex.loaders.fd12p
python -m GV_tools.gcpex.loaders.geonor
python -m GV_tools.gcpex.loaders.met
python -m GV_tools.gcpex.loaders.mrr
python -m GV_tools.gcpex.loaders.pip
python -m GV_tools.gcpex.loaders.pluvio
python -m GV_tools.gcpex.loaders.sonde
python -m GV_tools.gcpex.loaders.twodvd
python -m GV_tools.gcpex.loaders.vertix
python -m GV_tools.gcpex.loaders.wkr_polppi
