import GV_tools.gcpex.core.sites_info

#Example of an iterator (test_site_range) which can be used with
#nose and py.test to perform generative tests.  See for example
#https://www.ibm.com/developerworks/aix/library/au-pythontesting3/index.html?ca=drs-
#
#From the main GV_tools development directory, run
#$ nosetests
#or
#$ nosetests -v


#If a site is added in GV_tools/gcpex/sites_info.py, add entries
#with test values to distance_dict, and add the site to the sites
#list in test_site_range() below
distance_dict = {
    ('Mortons', 'Steamshow'): 16.055540,
    ('Mortons', 'Skydive'): 23.256344,
    ('Mortons', 'CARE'): 12.722043,
    ('Mortons', 'Huronia'): 56.681638,
    ('Steamshow', 'Skydive'): 8.878113,
    ('Steamshow', 'CARE'): 7.685554,
    ('Steamshow', 'Huronia'): 58.635202,
    ('Skydive', 'CARE'): 11.189874,
    ('Skydive', 'Huronia'): 54.829545,
    ('CARE', 'Huronia'): 51.711194}


def isclose(a, b, rel_tol=None, abs_tol=None):
    if rel_tol is None:
        rel_tol = 1.0e-9
    if abs_tol is None:
        abs_tol = 0.

    result = abs(a - b) < max(rel_tol*max(abs(a), abs(b)), abs_tol)
    return result

def check(start_loc, end_loc):
    distance = GV_tools.gcpex.core.sites_info.site_range(start_loc, end_loc)
    actual_distance = distance_dict[(start_loc.label, end_loc.label)]
    result = isclose(distance, actual_distance, rel_tol=1.0e-5)
    assert result == True
    
def test_site_range():
    sites = [GV_tools.gcpex.core.sites_info.mortons,
             GV_tools.gcpex.core.sites_info.steamshow,
             GV_tools.gcpex.core.sites_info.skydive,
             GV_tools.gcpex.core.sites_info.care,
             GV_tools.gcpex.core.sites_info.huronia]
    for i_loc, start_loc in enumerate(sites[:-1]):
        for end_loc in sites[i_loc+1:]:
            descriptor = 'Distance %s to %s' %(start_loc.label, end_loc.label)
            #if testing with py.test
            #yield descriptor, check, start_loc, end_loc
            # if testing with nose
            yield check, start_loc, end_loc
