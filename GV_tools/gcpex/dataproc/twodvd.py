#!/usr/bin/env python3


"""Processing for 2D Video Disdrometer data structures
"""

#--------------------------------------------------------------------------
# Processing for 2D Video Disdrometer data structures
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import sys
import numpy
from ..loaders.twodvd import TwoDVDData

_MISSING_FLOAT = -999.
_MISSING_INT = -999

def time_subsample(twodvd_data, t_start, t_end):
    crit = numpy.logical_and(t_start <= twodvd_data.times_utc,
                             twodvd_data.times_utc < t_end)
    n_times_subsample = numpy.sum(crit)
    if n_times_subsample > 0:
        twodvd_data_subsample = TwoDVDData(n_times = n_times_subsample,
                                           times_utc = twodvd_data.times_utc[crit],
                                           D_major = twodvd_data.D_major[crit], 
                                           fallspeed = twodvd_data.fallspeed[crit],
                                           sampling_area = twodvd_data.sampling_area[crit])
    else:
        twodvd_data_subsample = TwoDVDData(n_times = 0,
                                           times_utc = None,
                                           D_major = None,
                                           fallspeed = None,
                                           sampling_area = None)

    return twodvd_data_subsample

def get_V_D(twodvd_data, t_start=None, t_end=None):
    
   crit = numpy.logical_and(t_start <= twodvd_data.times_utc,
                            twodvd_data.times_utc < t_end)
   indices = numpy.where(crit)[0]
   N_samples = len(indices)
   if N_samples > 0:
      return(twodvd_data.D_major[indices], twodvd_data.fallspeed[indices])
   else:
      return(None, None)

def bin_V_D(twodvd_data, D_bin_lbounds, D_bin_widths, t_start=None, t_end=None):
    if t_start is not None and t_end is not None:
        twodvd_data_subsample = time_subsample(twodvd_data, t_start=t_start, t_end=t_end)
        D_sample = twodvd_data_subsample.D_major
        vt_sample = twodvd_data_subsample.fallspeed
    elif t_start is None and t_end is None:
        D_sample = twodvd_data.D_major
        vt_sample = twodvd_data.fallspeed
    else:
        #Inconsistency
        sys.stderr.write('Inconsistent None values for t_start, t_stop in twodvd.bin_V_D()\n')
        assert 0
        
    if D_sample is not None:
        N_samples_per_bin_min = 2   #Arbitrary
        #Bin the data
        N_bins = D_bin_lbounds.shape[0]
        vt_sample_bin_means = numpy.zeros((N_bins,),dtype=float)
        vt_sample_bin_sigmas = numpy.zeros((N_bins,),dtype=float)
        N_samples = numpy.zeros((N_bins,),dtype=int)
        #Use the bin bounds and widths to bin the fallspeed obs and produce mean and variance in each bin
        for i_bin in range(0,N_bins):
            crit = numpy.logical_and(D_bin_lbounds[i_bin] <= D_sample,
                                     D_sample < D_bin_lbounds[i_bin] + D_bin_widths[i_bin])
            indices = numpy.where(crit)[0]
            N_samples_subset = len(indices)
            if N_samples_subset >= N_samples_per_bin_min:
                vt_subset = vt_sample[indices]
                #With filtering
                (vt_mean, vt_variance, N_filtered) = filtered_mean_variance(vt_subset)
                vt_sigma = numpy.sqrt(vt_variance)
                N_samples[i_bin] = N_filtered
                #Without filtering
                #vt_mean = numpy.mean(vt_subset)
                #vt_sigma = numpy.std(vt_subset)
                #N_samples[i_bin] = N_samples_subset

                #Put the results in the arrays
                vt_sample_bin_means[i_bin] = vt_mean
                vt_sample_bin_sigmas[i_bin] = vt_sigma
            else:
                vt_sample_bin_means[i_bin] = 0.
                vt_sample_bin_sigmas[i_bin] = 0.

    else:
        vt_sample_bin_means = None
        vt_sample_bin_sigmas = None
        N_samples = None
       
    return(vt_sample_bin_means, vt_sample_bin_sigmas, N_samples)   

def filtered_mean_variance(vt_sample):
    N_samples = len(vt_sample)
    #Find the mode - the fallspeed that occurs most often in the sample
    #These are real, floating point values, so need to discretize
    #Round all to nearest 0.1 m/s
    vt_sample_round = numpy.round(vt_sample, decimals=1)
    #Use a dictionary to count occurrences
    mode_dict = {}
    for vt_val in vt_sample_round:
        try:
            mode_dict[vt_val] += 1
        except KeyError:
            mode_dict[vt_val] = 1
    vt_mode = None
    max_count = 0
    for key, value in mode_dict.items():
        if value > max_count:
            max_count = value
            vt_mode = key
    if vt_mode is not None:
        #Found a mode value, screen the values
        vt_low = vt_mode - 0.5
        vt_high = vt_mode + 0.5
        indices_filtered = numpy.where(numpy.logical_and(numpy.less(vt_low, vt_sample), numpy.less(vt_sample, vt_high)))[0]
        N_filtered = len(indices_filtered)
        vt_mean = numpy.mean(vt_sample[indices_filtered])
        vt_var = numpy.var(vt_sample[indices_filtered])
        vt_mean_var = vt_var/N_filtered   #sqrt of this is std err of the mean
    else:
        #Didn't find a mode, use the median
        vt_sample.sort()
        idx_mid = int(numpy.round(N_samples/2.))
        vt_median = vt_sample[idx_mid]
        vt_low = vt_median - 0.5
        vt_high = vt_median + 0.5
        indices_filtered = numpy.where(numpy.logical_and(numpy.less(vt_low, vt_sample), numpy.less(vt_sample, vt_high)))[0]
        N_filtered = len(indices_filtered)
        vt_mean = numpy.mean(vt_sample[indices_filtered])
        vt_var = numpy.var(vt_sample[indices_filtered])
    return(vt_mean, vt_var, N_filtered)
      
      

def V_stats(twodvd_data, D_bin_bounds, D_bin_widths, t_start=None, t_end=None, diagnostics=False):
    #D_bin_bounds should contain 3 values:  large D, mid D, small D
    #D_bin_widths should also contain 3 values:  width at large D, width at mid D, width at small D
    #Sample 2DVD data in each "bin", then compute means, sigmas and stats

    #21 April 2010 - I think I should be using standard error of the mean for 
    #the sigmas that are returned, since in the forward model I'm trying to simulate
    #the mean fallspeeds in each of the three size bins.  Test this - it means the retrieval
    #may have a more difficult time matching all the observations, since the uncertainties
    #on the observed V_stats will be smaller
    if t_start is not None and t_end is not None:
        twodvd_data_subsample = time_subsample(twodvd_data, t_start=t_start, t_end=t_end)
        D_sample = twodvd_data_subsample.D_major
        vt_sample = twodvd_data_subsample.fallspeed
    elif t_start is None and t_end is None:
        D_sample = twodvd_data.D_major
        vt_sample = twodvd_data.fallspeed
    else:
        #Inconsistency
        sys.stderr.write('Inconsistent None values for t_start, t_stop in twodvd.V_stats()\n')
        assert 0
        
    N_samples_per_bin_min = 2   #Arbitrary, but ensures std can be calculated
    if D_sample is not None:
        vt_mean = [None, None, None]
        vt_var = [None, None, None]
        vt_mean_var = [None, None, None]
        N_filtered = [None, None, None]
        for idx in range(3):
            crit = numpy.logical_and(numpy.less(D_bin_bounds[idx] - D_bin_widths[idx]/2., D_sample),
                                     numpy.less(D_sample, D_bin_bounds[idx] + D_bin_widths[idx]/2.))
            indices = numpy.where(crit)[0]
            N_samples = len(indices)
            if N_samples > N_samples_per_bin_min:
                vt_subset = vt_sample[indices]
                #Here's where we implement the Brandes et al (2008) filtering
                (vt_mean[idx], vt_var[idx], N_filtered[idx]) = filtered_mean_variance(vt_subset)
                vt_mean_var[idx] = vt_var[idx]/N_filtered[idx]

        if vt_mean[0] is not None and vt_mean[1] is not None and vt_mean[2] is not None:
            #See notes of 27 April to see how these were derived (especially the covariances)
            #The covariances of vt0 with either of the two Deltas is just the variance of vt0
            #Covariance between the two Deltas should also just be the variance of vt0
            S_eps_V_stats = numpy.matrix(numpy.zeros((3,3), dtype=float))
            #Variances
            S_eps_V_stats[0,0] = vt_mean_var[0]
            S_eps_V_stats[1,1] = vt_mean_var[0] + vt_mean_var[1]
            S_eps_V_stats[2,2] = vt_mean_var[0] + vt_mean_var[2]
            #Covariances
            S_eps_V_stats[0,1] = vt_mean_var[0]
            S_eps_V_stats[1,0] = vt_mean_var[0]
            S_eps_V_stats[0,2] = vt_mean_var[0]
            S_eps_V_stats[2,0] = vt_mean_var[0]
            S_eps_V_stats[1,2] = vt_mean_var[0]
            S_eps_V_stats[2,1] = vt_mean_var[0]
            
            V_stat_vals = (vt_mean[0], vt_mean[0] - vt_mean[1], vt_mean[0] - vt_mean[2])
            V_stat_counts = (N_filtered[0], N_filtered[1], N_filtered[2])
        else:
            S_eps_V_stats = None
            V_stat_vals = (None, None, None)
            V_stat_counts = (None, None, None)
    else:
        V_stat_vals = (None, None, None)
        V_stat_counts = (None, None, None)
        S_eps_V_stats = None
    if diagnostics == False:
        return(V_stat_vals, S_eps_V_stats, V_stat_counts)
    else:
        #return full
        return(V_stat_vals, S_eps_V_stats, V_stat_counts)

def size_dist(twodvd_data, D_bin_lbounds, D_bin_widths, t_start=None, t_end=None, diagnostics=False):
    #Expect D in mm, vt in m/s, area in m^2, dt in seconds
    if t_start is not None and t_end is not None:
        twodvd_data_subsample = time_subsample(twodvd_data, t_start=t_start, t_end=t_end)
        D_sample = twodvd_data_subsample.D_major
        vt_sample = twodvd_data_subsample.fallspeed
        sampling_area_sample = twodvd_data_subsample.sampling_area
        dt = (twodvd_data_subsample.times_utc[-1] - twodvd_data_subsample.times_utc[0])/numpy.timedelta64(1, 's')
    elif t_start is None and t_end is None:
        D_sample = twodvd_data.D_major
        vt_sample = twodvd_data.fallspeed
        sampling_area_sample = twodvd_data.sampling_area
        dt = (twodvd_data.times_utc[-1] - twodvd_data.times_utc[0])/numpy.timedelta64(1, 's')
    else:
        #Inconsistency
        sys.stderr.write('Inconsistent None values for t_start, t_stop in twodvd.V_stats()\n')
        assert 0
    
    N_samples = D_sample.shape[0]
    N_bins = D_bin_lbounds.shape[0]
    counts = numpy.zeros((N_bins,), dtype=int)
    N_D = numpy.zeros((N_bins,), dtype=float)
    N_D_var = numpy.zeros((N_bins,), dtype=float)
    vt_sigmas = numpy.zeros((N_samples,), dtype=float)
    indices = {}

    #Bin the data
    for i_bin in range(N_bins):
        crit = numpy.logical_and(D_bin_lbounds[i_bin] < D_sample,
                                 D_sample < D_bin_lbounds[i_bin] + D_bin_widths[i_bin])
        selected_indices = numpy.where(crit)[0]
        if selected_indices is not None:
            counts[i_bin] = len(selected_indices)
            indices[i_bin] = selected_indices
        
    #Check to see if this is a good sample - want six or more consecutive occupied bins
    consec_count = 0
    consec_count_max = 0
    for i_bin in range(N_bins):
        if counts[i_bin] > 0:
            consec_count += 1
            consec_count_max = max(consec_count, consec_count_max)
        else:
            consec_count = 0

    valid = False
    if consec_count_max > 6:
       valid = True
       #Compute the size distribution in the bins for which the sample count is greater than 1
       select_bins = numpy.where(counts > 1)[0]
       for i_bin in select_bins:
           N_D[i_bin] = 1./(dt*D_bin_widths[i_bin])*numpy.sum(1./(sampling_area_sample[indices[i_bin]]*vt_sample[indices[i_bin]]))  #m^-3 mm^-1
           
           #Compute the mean and variance of the fallspeeds in the bin
           vt_bin_mean = numpy.mean(vt_sample[indices[i_bin]])
           vt_bin_sigma = numpy.std(vt_sample[indices[i_bin]])
           #Use a max of 40% uncertainty
           if vt_bin_sigma > 0.4*vt_bin_mean:
               vt_bin_sigma = 0.4*vt_bin_mean

           #Estimate a 20% uncertainty in the actaul bin width    
           mid_bin_size = D_bin_lbounds[i_bin] + D_bin_widths[i_bin]/2.
           bin_width_sigma = 0.20*mid_bin_size

           tmp = 1./counts[i_bin] + numpy.power(vt_bin_sigma/vt_bin_mean, 2) + numpy.power(bin_width_sigma/D_bin_widths[i_bin], 2)
           N_D_var[i_bin] = N_D[i_bin]*N_D[i_bin]*tmp

           vt_sigmas[indices[i_bin]] = vt_bin_sigma

    return(N_D, N_D_var, vt_sigmas, valid)

def Nt(twodvd_data, t_start=None, t_end=None):
    #Expect D in mm, vt in m/s, area in m^2, dt in seconds
    if t_start is not None and t_end is not None:
        twodvd_data_subsample = time_subsample(twodvd_data, t_start=t_start, t_end=t_end)
        D_sample = twodvd_data_subsample.D_major
        vt_sample = twodvd_data_subsample.fallspeed
        sampling_area_sample = twodvd_data_subsample.sampling_area
        dt = (twodvd_data_subsample.times_utc[-1] - twodvd_data_subsample.times_utc[0])/numpy.timedelta64(1, 's')
    elif t_start is None and t_end is None:
        D_sample = twodvd_data.D_major
        vt_sample = twodvd_data.fallspeed
        sampling_area_sample = twodvd_data.sampling_area
        dt = (twodvd_data.times_utc[-1] - twodvd_data.times_utc[0])/numpy.timedelta64(1, 's')
    else:
        #Inconsistency
        sys.stderr.write('Inconsistent None values for t_start, t_stop in twodvd.Nt()\n')
        assert 0

    N_samples = D_sample.shape[0]
    Nt = 0.
    for i_size in range(N_samples):
        Nt += 1./(vt_sample[i_size]*sampling_area_sample[i_size]*dt)
    return Nt

def test():
    import sys
    sys.path.insert(0, '../../')
    import gcpex.loaders.twodvd
    fpath = sys.argv[1]  #Like 2dvd_sn37_gcpex_CARE_N441359.45_W0794650.11_20120130.flakes_rematch.txt
    twodvd_data = gcpex.loaders.twodvd.load(fpath)
    t_start = twodvd_data.times_utc[0]
    t_end = twodvd_data.times_utc[-1]
    
    D_major_sample, vt_sample = get_V_D(twodvd_data, numpy.datetime64('0000-01-01T00:00:00'), numpy.datetime64('9999-12-12T23:59:59'))
    width = 0.25
    mybounds = numpy.arange(101, dtype=float)*0.25
    mywidths = numpy.copy(mybounds)
    mywidths[:] = width
    mywidths[-1] = 0.
    #Subset 5 minute samples, compute vtx_means & vtx_sigmas
    t_start = twodvd_data.times_utc[0]
    t_end = twodvd_data.times_utc[-1]
    t_bin_bounds = numpy.arange(t_start, t_end, numpy.timedelta64(5, 'm'))

    #These match the forward model settings
    D_vtx_stats_mm = numpy.array((4.0, 2.0, 1.0), dtype=float)
    #Widen the sample width a bit for the largest sizes.  For distributions
    #with a high slope, might not have enough samples at the large size
    #widths_vtx_stats_mm = numpy.array((0.25, 0.25, 0.25), dtype=float)
    widths_vtx_stats_mm = numpy.array((0.5, 0.25, 0.25), dtype=float)

    print("#           V_stat_1                     V_stat_2                     V_stat_3")
    #print("#               V_1                        V_2                           V_3")
    print("#      Mean            Sigma        Mean           Sigma         Mean            Sigma")
    for i_time, t_bin_start in enumerate(t_bin_bounds[:-1]):
        t_bin_end = t_bin_bounds[i_time+1]
        (V_stats_means, S_eps_V_stats, V_stats_counts) = V_stats(twodvd_data, D_vtx_stats_mm, widths_vtx_stats_mm,
                                                                 t_start=t_bin_start, t_end=t_bin_end)
        if numpy.any(numpy.equal(V_stats_means, None)):
            pass
        else:
            print("%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e" %(V_stats_means[0], numpy.sqrt(S_eps_V_stats[0,0]), \
                                                                V_stats_means[1], numpy.sqrt(S_eps_V_stats[1,1]), \
                                                                V_stats_means[2], numpy.sqrt(S_eps_V_stats[2,2]) ))

    
    (vtx_means, vtx_sigmas, N_samples) = bin_V_D(twodvd_data, mybounds, mywidths, t_start=t_start, t_end=t_end)

    (N_D, N_D_var, vt_sigmas, valid) = size_dist(twodvd_data, mybounds, mywidths, t_start=t_start, t_end=t_end)

    print("#   D_bin            mean f             sigma f         N_samples        N_D         var(N_D)")
    for i_size in range(len(mybounds)):
        print("%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e" %(mybounds[i_size],
                                                            vtx_means[i_size],
                                                            vtx_sigmas[i_size],
                                                            N_samples[i_size],
                                                            N_D[i_size],
                                                            N_D_var[i_size]))


if __name__ == "__main__":
    test()
