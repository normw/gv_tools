import numpy


"""Processing for Geonor data structures
"""

#--------------------------------------------------------------------------
# Processing for Geonor data structures
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


def calc_accum(geonor_data, t_start=None, t_end=None, t_tolerance=None):
    accum = None

    if t_tolerance is None:
        t_tol = numpy.timedelta64(30, 's')
    else:
        t_tol = t_tolerance

    if t_start is not None and t_end is not None:
        mask_start = numpy.logical_and(numpy.less_equal(t_start - t_tol, geonor_data.times_utc),
                                       numpy.less_equal(geonor_data.times_utc, t_start + t_tol))
        mask_end = numpy.logical_and(numpy.less_equal(t_end - t_tol, geonor_data.times_utc),
                                       numpy.less_equal(geonor_data.times_utc, t_end + t_tol))
        if numpy.sum(mask_start) > 0 and numpy.sum(mask_end) > 0:
            indices_start = numpy.where(mask_start)[0]
            indices_end = numpy.where(mask_end)[0]
            idx_start = numpy.amin(indices_start)
            idx_end = numpy.amax(indices_end)
            accum = geonor_data.accumulation[idx_end] - geonor_data.accumulation[idx_start]
    return accum
