import numpy


"""Processing for FD12P data structures
"""

#--------------------------------------------------------------------------
# Processing for FD12P data structures
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


def subsample_time(data, t_start=None, t_end=None, t_tolerance=None):
    idx_start = None
    idx_end = None

    if t_tolerance is not None:
        t_tol = t_tolerance
    else:
        t_tol = numpy.timedelta64(30, 's')

    mask_start = numpy.logical_and(numpy.less_equal(t_start - t_tol, data.times_utc),
                                   numpy.less(data.times_utc, t_start + t_tol))
    mask_end = numpy.logical_and(numpy.less_equal(t_end - t_tol, data.times_utc),
                                   numpy.less(data.times_utc, t_end + t_tol))
    if numpy.any(mask_start):
        indices = numpy.where(mask_start)[0]
        idx_start = numpy.amin(indices)

    if numpy.any(mask_end):
        indices = numpy.where(mask_end)[0]
        idx_end = numpy.amax(indices)

    return (idx_start, idx_end)

def calc_accum(fd12p_data, t_start=None, t_end=None, t_tolerance=None):
    accum = None
    if t_start is not None and t_end is not None:
        (idx_start, idx_end) = subsample_time(fd12p_data, t_start=t_start, t_end=t_end, t_tolerance=t_tolerance)
        if idx_start is not None and idx_end is not None:
            accum = numpy.sum(fd12p_data.precip_rate[idx_start:idx_end]*fd12p_data.measurement_interval_h)
    return accum
