#!/usr/bin/env python3

"""Processing for 2DVD data structures

Copyright (c) 2017 Norman Wood.  All rights reserved.

This file is part of GV_tools.  It is subject to the license terms of
the BSD-3-Clause license, the full text of which is available in the
LICENSE file found in the top-level directory of the GV_tools
distribution.  No part of GV_tools, including this file, may be copied,
modified, propagated, or distributed except according to the terms
contained in the LICENSE file.
"""

import string, numpy

frac_doy_2dvd = None
D_major_mm_2dvd = None
D_ratio_2dvd = None
vtx_ms_2dvd = None


def init(filename):
   
   global frac_doy_2dvd, D_major_mm_2dvd, D_ratio_2dvd, vtx_ms_2dvd

   yy = int(filename[-11:-9])
   doy_2dvd = float(filename[-9:-6])
   if yy == 7:
       doy_2dvd = doy_2dvd + 365.

   f_in = open(filename)
   lines = f_in.readlines()
   f_in.close()

   frac_doy_list = []
   D_major_list = []
   D_ratio_list = []
   vtx_list = []

   for line in lines:
      if line[0] != '#':
         data = map(float,string.splitfields(line))
         scan_index = data[0]
         time_UTC_s = scan_index*36./2.e6
         frac_doy_list.append(doy_2dvd + time_UTC_s/86400.)
         shadow_area_A = data[4]
         shadow_area_B = data[5]
         width_A = data[7]
         width_B = data[9]

         ave_shadow_area = (shadow_area_A + shadow_area_B)/2.
         #Dcircle = numpy.sqrt(ave_shadow_area*4./numpy.pi)
         Dcircle = numpy.sqrt(shadow_area_A*4./numpy.pi)
         
         D_major_val = numpy.maximum(width_A, width_B)
         try:
            D_ratio = Dcircle/D_major_val
         except:
            print(Dcircle, D_major_val)
            print(line)
            assert 0
         D_ratio_list.append(D_ratio)
         D_major_list.append(D_major_val)
         vtx_list.append(data[2])

   frac_doy_2dvd = numpy.array(frac_doy_list, dtype=float)
   D_major_mm_2dvd = numpy.array(D_major_list, dtype=float)
   D_ratio_2dvd = numpy.array(D_ratio_list, dtype=float)
   vtx_ms_2dvd = numpy.array(vtx_list, dtype=float)
   del frac_doy_list, D_major_list, D_ratio_list,vtx_list

   return


def get_V_D(target_doy_start, target_doy_end):
   crit = numpy.logical_and(target_doy_start <= frac_doy_2dvd, frac_doy_2dvd <= target_doy_end)
   indices = numpy.where(crit)[0]
   N_samples = len(indices)
   if N_samples > 0:
      return(D_major_mm_2dvd[indices], vtx_ms_2dvd[indices])
   else:
      return(None, None)

def get_D_ratio(target_doy_start, target_doy_end):
   crit = numpy.logical_and(target_doy_start <= frac_doy_2dvd, frac_doy_2dvd <= target_doy_end)
   indices = numpy.where(crit)[0]
   N_samples = len(indices)
   if N_samples > 0:
      return(D_major_mm_2dvd[indices], D_ratio_2dvd[indices])
   else:
      return(None, None)


def bin_V_D(bin_bounds, bin_widths, target_doy_start, target_doy_end):
   (D_major_mm_sample, vtx_ms_sample) = get_V_D(target_doy_start, target_doy_end)

   if D_major_mm_sample != None:
      N_samples_per_bin_min = 2   #Arbitrary
      #Bin the data
      N_bins = bin_bounds.shape[0]
      vt_sample_bin_means = numpy.zeros((N_bins,),dtype=float)
      vt_sample_bin_sigmas = numpy.zeros((N_bins,),dtype=float)
      N_samples = numpy.zeros((N_bins,),dtype=int)
      #Use the bin bounds and widths to bin the fallspeed obs and produce mean and variance in each bin
      #Why did I start at 1?
      #for i_bin in range(1,N_bins):
      for i_bin in range(0,N_bins):
         crit = numpy.logical_and(bin_bounds[i_bin] <= D_major_mm_sample, D_major_mm_sample < bin_bounds[i_bin] + bin_widths[i_bin])
         indices = numpy.where(crit)[0]
         N_samples[i_bin] = len(indices)
         if N_samples[i_bin] >= N_samples_per_bin_min:
            vt_subset = vtx_ms_sample[indices]
            #With filtering
            (vt_mean, vt_variance, N_filtered) = filtered_mean_variance(vt_subset)
            N_samples[i_bin] = N_filtered
            #Without filtering
            #vt_mean = numpy.mean(vt_subset)
            #vt_sigma = numpy.std(vt_subset)

            #Put the results in the arrays
            vt_sample_bin_means[i_bin] = vt_mean
            vt_sample_bin_sigmas[i_bin] = numpy.sqrt(vt_variance)
         else:
            vt_sample_bin_means[i_bin] = 0.
            vt_sample_bin_sigmas[i_bin] = 0.

   else:
      vt_sample_bin_means = None
      vt_sample_bin_sigmas = None
      N_samples = None
      
   return(vt_sample_bin_means, vt_sample_bin_sigmas, N_samples)   

def bin_D_ratio(bin_bounds, bin_widths, target_doy_start, target_doy_end):
   (D_major_mm_sample, D_ratio_sample) = get_D_ratio(target_doy_start, target_doy_end)

   if D_major_mm_sample != None:
      N_samples_per_bin_min = 2   #Arbitrary
      #Bin the data
      N_bins = bin_bounds.shape[0]
      D_ratio_sample_bin_means = numpy.zeros((N_bins,),dtype=float)
      D_ratio_sample_bin_sigmas = numpy.zeros((N_bins,),dtype=float)
      N_samples = numpy.zeros((N_bins,),dtype=int)
      #Use the bin bounds and widths to bin the fallspeed obs and produce mean and variance in each bin
      #Why did I start at 1?
      #for i_bin in range(1,N_bins):
      for i_bin in range(0,N_bins):
         crit = numpy.logical_and(bin_bounds[i_bin] <= D_major_mm_sample, D_major_mm_sample < bin_bounds[i_bin] + bin_widths[i_bin])
         indices = numpy.where(crit)[0]
         N_samples[i_bin] = len(indices)
         if N_samples[i_bin] >= N_samples_per_bin_min:
            D_ratio_subset = D_ratio_sample[indices]
            D_ratio_mean = numpy.mean(D_ratio_subset)
            D_ratio_sigma = numpy.std(D_ratio_subset)
            D_ratio_sample_bin_means[i_bin] = D_ratio_mean
            D_ratio_sample_bin_sigmas[i_bin] = D_ratio_sigma
         else:
            D_ratio_sample_bin_means[i_bin] = 0.
            D_ratio_sample_bin_sigmas[i_bin] = 0.

   else:
      D_ratio_sample_bin_means = None
      D_ratio_sample_bin_sigmas = None
      N_samples = None
      
   return(D_ratio_sample_bin_means, D_ratio_sample_bin_sigmas, N_samples)   

def mean_V(target_doy_start, target_doy_end):
   (D_major_mm_sample, vtx_ms_sample) = get_V_D(target_doy_start, target_doy_end)

   #Compute the simple mean over all observations
   mean_val = numpy.mean(vtx_ms_sample)

   return mean_val


def filtered_mean_variance(vt_sample):
   N_samples = len(vt_sample)
   #Find the mode - the fallspeed that occurs most often in the sample
   #These are real, floating point values, so need to discretize
   #Round all to nearest 0.1 m/s
   vt_sample_round = numpy.round(vt_sample, decimals=1)
   #Use a dictionary to count occurrences
   mode_dict = {}
   for vt_val in vt_sample_round:
      try:
         mode_dict[vt_val] += 1
      except KeyError:
         mode_dict[vt_val] = 1
   vt_mode = None
   max_count = 0
   for key, value in mode_dict.items():
      if value > max_count:
         max_count = value
         vt_mode = key
   if vt_mode != None:
      #Found a mode value, screen the values
      vt_low = vt_mode - 0.5
      vt_high = vt_mode + 0.5
      indices_filtered = numpy.where(numpy.logical_and(numpy.less(vt_low, vt_sample), numpy.less(vt_sample, vt_high)))[0]
      N_filtered = len(indices_filtered)
      vt_mean = numpy.mean(vt_sample[indices_filtered])
      vt_var = numpy.var(vt_sample[indices_filtered])
      vt_mean_var = vt_var/N_filtered   #sqrt of this is std err of the mean
   else:
      #Didn't find a mode, use the median
      vt_sample.sort()
      idx_mid = int(numpy.round(N_samples/2.))
      vt_median = vt_sample[idx_mid]
      vt_low = vt_median - 0.5
      vt_high = vt_median + 0.5
      indices_filtered = numpy.where(numpy.logical_and(numpy.less(vt_low, vt_sample), numpy.less(vt_sample, vt_high)))[0]
      N_filtered = len(indices_filtered)
      vt_mean = numpy.mean(vt_sample[indices_filtered])
      vt_var = numpy.var(vt_sample[indices_filtered])
   return(vt_mean, vt_var, N_filtered)
      
      

def V_stats(D_mm, width_mm, target_doy_start, target_doy_end, diagnostics=False):
   #D_mm should contain 3 values:  large D, mid D, small D
   #widths_mm should also contain 3 values:  width at large D, width at mid D, width at small D
   #Sample 2DVD data in each "bin", then compute means, sigmas and stats

   #21 April 2010 - I think I should be using standard error of the mean for 
   #the sigmas that are returned, since in the forward model I'm trying to simulate
   #the mean fallspeeds in each of the three size bins.  Test this - it means the retrieval
   #may have a more difficult time matching all the observations, since the uncertainties
   #on the observed V_stats will be smaller
   N_samples_per_bin_min = 2   #Arbitrary, but ensures std can be calculated
   (D_major_mm_sample, vtx_ms_sample) = get_V_D(target_doy_start, target_doy_end)
   if D_major_mm_sample != None:
      crit = numpy.logical_and(numpy.less(D_mm[0] - width_mm[0]/2., D_major_mm_sample), numpy.less(D_major_mm_sample,D_mm[0] + width_mm[0]/2.))
      indices = numpy.where(crit)[0]
      N_samples0 = len(indices)
      if N_samples0 > N_samples_per_bin_min:
         vt_subset = vtx_ms_sample[indices]
         #Here's where we implement the Brandes et al (2008) filtering
         (vt0_mean, vt0_var, N_filtered_0) = filtered_mean_variance(vt_subset)
         vt0_mean_var = vt0_var/N_filtered_0
         #vt0_mean_var = vt0_var
      else:
         vt0_mean = None
         vt0_var = None
         vt0_mean_var = None

      crit = numpy.logical_and(D_mm[1] - width_mm[1]/2. < D_major_mm_sample, D_major_mm_sample < D_mm[1] + width_mm[1]/2.)
      indices = numpy.where(crit)[0]
      N_samples1 = len(indices)
      if N_samples1 > N_samples_per_bin_min:
         vt_subset = vtx_ms_sample[indices]
         #Here's where we implement the Brandes et al (2008) filtering
         (vt1_mean, vt1_var, N_filtered_1) = filtered_mean_variance(vt_subset)
         vt1_mean_var = vt1_var/N_filtered_1
         #vt1_mean_var = vt1_var
      else:
         vt1_mean = None
         vt1_var = None
         vt1_mean_var = None
   
      crit = numpy.logical_and(D_mm[2] - width_mm[2]/2. < D_major_mm_sample, D_major_mm_sample < D_mm[2] + width_mm[2]/2.)
      indices = numpy.where(crit)[0]
      N_samples2 = len(indices)
      if N_samples2 > N_samples_per_bin_min:
         vt_subset = vtx_ms_sample[indices]
         #Here's where we implement the Brandes et al (2008) filtering
         (vt2_mean, vt2_var, N_filtered_2) = filtered_mean_variance(vt_subset)
         vt2_mean_var = vt2_var/N_filtered_2
         #vt2_mean_var = vt2_var
      else:
         vt2_mean = None
         vt2_var = None
         vt2_mean_var = None

      if vt0_mean != None and vt1_mean != None and vt2_mean != None:
         #See notes of 27 April to see how these were derived (especially the covariances)
         #The covariances of vt0 with either of the two Deltas is just the variance of vt0
         #Covariance between the two Deltas should also just be the variance of vt0
         S_eps_V_stats = numpy.matrix(numpy.zeros((3,3), dtype=float))
         #Variances
         S_eps_V_stats[0,0] = vt0_mean_var
         S_eps_V_stats[1,1] = vt0_mean_var + vt1_mean_var
         S_eps_V_stats[2,2] = vt0_mean_var + vt2_mean_var
         #Covariances
         S_eps_V_stats[0,1] = vt0_mean_var
         S_eps_V_stats[1,0] = vt0_mean_var
         S_eps_V_stats[0,2] = vt0_mean_var
         S_eps_V_stats[2,0] = vt0_mean_var
         S_eps_V_stats[1,2] = vt0_mean_var
         S_eps_V_stats[2,1] = vt0_mean_var
         
         V_stat_vals = (vt0_mean, vt0_mean - vt1_mean, vt0_mean - vt2_mean)
         V_stat_counts = (N_filtered_0, N_filtered_1, N_filtered_2)
      else:
         S_eps_V_stats = None
         V_stat_vals = (None, None, None)
         V_stat_counts = (None, None, None)
   else:
      V_stat_vals = (None, None, None)
      V_stat_counts = (None, None, None)
      S_eps_V_stats = None
   if diagnostics == False:
      return(V_stat_vals, S_eps_V_stats, V_stat_counts)
   else:
      #return full
      return(V_stat_vals, S_eps_V_stats, V_stat_counts)

if __name__ == '__main__':
   def mytest0(infile):
      init(infile)
      (D_major_sample, vtx_sample) = get_V_D(-999., 999.)
      width = 0.25
      mybounds = numpy.arange(101, dtype=float)*0.25
      mywidths = numpy.copy(mybounds)
      mywidths[:] = width
      mywidths[-1] = 0.
      #Subset 5 minute samples, compute vtx_means & vtx_sigmas
      frac_doy_start = frac_doy_2dvd[0]
      frac_doy_interval = 5./1440.
      frac_doy_end = frac_doy_2dvd[-1] - frac_doy_interval
      frac_doy_end = max(frac_doy_start + frac_doy_interval, frac_doy_2dvd[-1] - frac_doy_interval)
      print("#  ",frac_doy_start, frac_doy_end)
      #These match the forward model settings
      D_vtx_stats_mm = numpy.array((4.0, 2.0, 1.0), dtype=float)
      #Widen the sample width a bit for the largest sizes.  For distributions
      #with a high slope, might not have enough samples at the large size
      #widths_vtx_stats_mm = numpy.array((0.25, 0.25, 0.25), dtype=float)
      widths_vtx_stats_mm = numpy.array((0.5, 0.25, 0.25), dtype=float)

      print("#           V_stat_1                     V_stat_2                     V_stat_3")
      #print("#               V_1                        V_2                           V_3")
      print("#      Mean            Sigma        Mean           Sigma         Mean            Sigma")
      while frac_doy_start <= frac_doy_end:
         #full_results =  V_stats(D_vtx_stats_mm, widths_vtx_stats_mm, frac_doy_start, frac_doy_start+frac_doy_interval, diagnostics=True)
         #if numpy.any(numpy.equal(full_results, None)):
         #   pass
         #else:
         #   print("%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e" %(full_results[0], full_results[1], \
         #                                                       full_results[2], full_results[3], \
         #                                                       full_results[4], full_results[5]))
 
         (V_stats_means, S_eps_V_stats, V_stats_counts) = V_stats(D_vtx_stats_mm, widths_vtx_stats_mm, frac_doy_start, frac_doy_start+frac_doy_interval)
         if numpy.any(numpy.equal(V_stats_means, None)):
            pass
         else:
            print("%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e" %(V_stats_means[0], numpy.sqrt(S_eps_V_stats[0,0]), \
                                                                V_stats_means[1], numpy.sqrt(S_eps_V_stats[1,1]), \
                                                                V_stats_means[2], numpy.sqrt(S_eps_V_stats[2,2]) ))
            #numpy.set_printoptions(linewidth=132)
            #print(S_eps_V_stats)
            #numpy.set_printoptions(linewidth=75)

         frac_doy_start += frac_doy_interval

   def mytest1(infile):
      init(infile)
      (D_major_sample, vtx_sample) = get_V_D(-999., 999.)
      width = 0.25
      mybounds = numpy.arange(101, dtype=float)*0.25
      mywidths = numpy.copy(mybounds)
      mywidths[:] = width
      mywidths[-1] = 0.
      (vtx_means, vtx_sigmas, N_samples) = bin_V_D(mybounds, mywidths, -999., 999.)
      (D_ratio_means, D_ratio_sigmas, N_samples) = bin_D_ratio(mybounds, mywidths, -999., 999.)
      print("#   D_bin            mean f             sigma f         N_samples")
      for i_size in range(len(mybounds)):
      #   print("%15.5e %15.5e %15.5e %15.5e" %(mybounds[i_size], means[i_size],sigmas[i_size], N_samples[i_size]))
         print("%15.5e %15.5e %15.5e %15.5e" %(mybounds[i_size], D_ratio_means[i_size], D_ratio_sigmas[i_size], N_samples[i_size]))

   import sys
   infile = sys.argv[1]
   mytest0(infile)             
