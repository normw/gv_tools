
"""Processing for meteorology data structures
"""

#--------------------------------------------------------------------------
# Processing for meteorology data structures
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

import numpy

def wind_dir_ave(u_scalar, phi):
    N_samples = u_scalar.shape[0]
    u_ave = numpy.ma.average(-u_scalar*numpy.sin(phi*numpy.pi/180.))
    v_ave = numpy.ma.average(-u_scalar*numpy.cos(phi*numpy.pi/180.))
    phi_vector_ave = numpy.ma.arctan2(u_ave, v_ave)*180./numpy.pi + 180.

    u_vector_ave = numpy.ma.sqrt(u_ave**2 + v_ave**2)

    print(u_scalar, phi,)
    print(u_vector_ave, phi_vector_ave)
    return (u_vector_ave, phi_vector_ave) 

def time_average(met_data, t_start=None, t_end=None):

    time_utc = None
    u_2m_ave = None
    u_10m_ave = None
    phi_10m_ave = None
    T_ave = None
    P_ave = None
    RH_ave = None
    T_corr_ave = None
    P_corr_ave = None

    
    if t_start is not None and t_end is not None:
        mask = numpy.logical_and(numpy.less_equal(t_start, met_data.times_utc),
                                 numpy.less(met_data.times_utc, t_end))
        indices = numpy.where(mask)[0]
        if len(indices) > 0:
            time_utc = t_start
            u_2m_ave = numpy.ma.average(met_data.u_2m[indices])
            u_10m_ave = numpy.ma.average(met_data.u_10m[indices])
            #phi_10m_ave = numpy.ma.average(met_data.phi_10m[indices])
            u_10m_vector_ave, phi_10m_ave = wind_dir_ave(met_data.u_10m[indices], met_data.phi_10m[indices])
            T_ave = numpy.ma.average(met_data.T[indices])
            P_ave = numpy.ma.average(met_data.P[indices])
            RH_ave = numpy.ma.average(met_data.RH[indices])
            T_corr_ave = numpy.ma.average(met_data.T_corr[indices])
            P_corr_ave = numpy.ma.average(met_data.P_corr[indices])

    return (time_utc, u_2m_ave, u_10m_ave, phi_10m_ave, T_ave, P_ave, RH_ave, T_corr_ave, P_corr_ave)
