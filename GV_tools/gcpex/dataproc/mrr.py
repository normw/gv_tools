
"""Processing for Micro Rain Radar data structures
"""

#--------------------------------------------------------------------------
# Processing for Micro Rain Radar data structures
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import numpy

def time_average(mrr_data, t_start=None, t_end=None):

    height_ave = None
    refl_ave = None
    v_doppler_ave = None

    if t_start is not None and t_end is not None:
        mask = numpy.logical_and(numpy.less_equal(t_start, mrr_data.times_utc),
                                 numpy.less(mrr_data.times_utc, t_end))
        indices = numpy.where(mask)[0]
        if len(indices) > 0:
            refl_vals = mrr_data.refl[indices, :]
            v_doppler_vals = mrr_data.v_doppler[indices, :]
            height_vals = mrr_data.heights[indices, :]

            height_stddev = numpy.ma.std(height_vals, axis=0)
            height_ave = numpy.ma.average(height_vals, axis=0)
            refl_linear = numpy.ma.power(10., refl_vals/10.)
            refl_linear_ave = numpy.ma.average(refl_linear, axis=0)
            refl_ave = 10.*numpy.ma.log10(refl_linear_ave)

            v_doppler_ave_num = numpy.ma.sum(v_doppler_vals*refl_linear, axis=0)
            v_doppler_ave_denom = numpy.ma.sum(refl_linear, axis=0)
            
            v_doppler_ave = v_doppler_ave_num/v_doppler_ave_denom

    return (height_ave, refl_ave, v_doppler_ave)
