#!/usr/bin/env python3

import collections
import math
import numpy


"""GCPEx site location information
"""

#--------------------------------------------------------------------------
# GCPEx site location information
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


#lat in decimal degrees north
#lon in decimal degrees east
#elevation in meters above mean sea level

def _distance(phi_s, lambda_s, phi_f, lambda_f):
   #The central angle formula is from the Wikipedia page for great circle distance,
   #and is the Vincenty formula applied to a sphere
   #phi_s, lambda_s are the lat and lon of the starting points, radians
   #phi_f, lambda_f are the lat and lon of the ending points, radians

   Earth_radius = 6.37101e3     #kilometers
   delta_lambda = lambda_f - lambda_s
   delta_phi = phi_f - phi_s
   num_term1 = numpy.power(numpy.cos(phi_f)*numpy.sin(delta_lambda),2)
   num_term2 = numpy.power(numpy.cos(phi_s)*numpy.sin(phi_f) - numpy.sin(phi_s)*numpy.cos(phi_f)*numpy.cos(delta_lambda),2)
   num = numpy.sqrt(num_term1 + num_term2)
   denom = numpy.sin(phi_s)*numpy.sin(phi_f) + numpy.cos(phi_s)*numpy.cos(phi_f)*numpy.cos(delta_lambda)
   central_angle = numpy.arctan2(num,denom)
   distance = Earth_radius*central_angle
   return distance

def site_range(site_1, site_2):
   d2r = math.pi/180.
   phi_s = site_1.lat*d2r
   lambda_s = site_1.lon*d2r
   phi_f = site_2.lat*d2r
   lambda_f = site_2.lon*d2r
   site_range = _distance(phi_s, lambda_s, phi_f, lambda_f)

   return site_range


SiteInfo = collections.namedtuple('SiteInfo', 'label, lat, lon, elevation')

sites = { 
'CARE': SiteInfo(label = 'CARE',
                lat = 44.2331806,
                lon = -79.7805861,
                elevation = 251.),

'Huronia': SiteInfo(label = 'Huronia',
                   lat = 44.6861806,
                   lon = -79.9279444,
                   elevation = 235.),

'Mortons': SiteInfo(label = 'Mortons',
                   lat = 44.1764694,
                   lon = -79.9192028,
                   elevation = 260.),

'Skydive': SiteInfo(label = 'Skydive',
                   lat = 44.2378611,
                   lon = -79.6402833,
                   elevation = 236.),

'Steamshow': SiteInfo(label = 'Steamshow',
                     lat = 44.1806306,
                     lon = -79.7179528,
                     elevation = 231.)}
