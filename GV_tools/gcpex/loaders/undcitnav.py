#!/usr/bin/env python3


"""Structure and methods for UND Citation navigation data
"""

#--------------------------------------------------------------------------
# Structure and methods for UND Citation naviation data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import numpy
import collections

_MISSING_FLOAT = -999.
_MISSING_INT = -999


#File properties
_N_DATA_ELEMENTS = 45
_N_MISSING_VALUE_FLAG_ELEMENTS = 44
_LINENUM_BASE_DATE = 6
_LINENUM_INVALID_FLAGS = 11
_LINENUM_DATA_START = 62

CitationNavData = collections.namedtuple('CitationNavData', ['n_times',
                                                             'times_utc',
                                                             'latitude',
                                                             'longitude',
                                                             'altitude',
                                                             'airspeed'])

def _extract_missing_flag_values(line):
    data = None
    elements = line.split()
    if len(elements) == _N_MISSING_VALUE_FLAG_ELEMENTS:
        #Proper data line
        airspeed = float(elements[2])
        lat = float(elements[17])
        lon = float(elements[18])
        alt = float(elements[19])

        data = (airspeed, lat, lon, alt)

    return data


def _extract_nav(line):
    data = None
    elements = line.split()
    if len(elements) == _N_DATA_ELEMENTS:
        #Proper data line
        time_s = float(elements[0])
        airspeed = float(elements[3])
        lat = float(elements[18])
        lon = float(elements[19])
        alt = float(elements[20])
        data = (time_s, airspeed, lat, lon, alt)
    return data


def _load_single(fpath):

    data_tuple = None
    time_list = []
    airspeed_list = []
    lat_list = []
    lon_list = []
    alt_list = []

    with open(fpath, 'r') as f_in:
        lines = f_in.readlines()

    #Get the date of data collections, line 6
    #and create a corresponding datetime64 object
    elements = lines[_LINENUM_BASE_DATE].split()
    year = int(elements[0])
    month = int(elements[1])
    day = int(elements[2])

    time_base = numpy.datetime64('%4d-%02d-%02dT00:00:00' %(year, month, day))

    #Get the list of values that indicate missing data
    missing_flag_values =  _extract_missing_flag_values(lines[_LINENUM_INVALID_FLAGS])

    (missing_airspeed,
     missing_lat,
     missing_lon,
     missing_alt) = missing_flag_values


    for line in lines[_LINENUM_DATA_START:]:
        data = _extract_nav(line)
        if data is not None:
            (time_s, airspeed, lat, lon, alt) = data
            time_list.append(time_s)
            airspeed_list.append(airspeed)
            lat_list.append(lat)
            lon_list.append(lon)
            alt_list.append(alt)

    n_times = len(time_list)

    offset_frac, offset_int = numpy.modf(time_list)
    offset_parts = zip(offset_int, offset_frac)

    time_deltas = [numpy.timedelta64(int(x[0]),'s') + numpy.timedelta64(int(x[1]*1000.), 'ms') for x in offset_parts]
    times_utc = time_base + time_deltas

    data_tuple = CitationNavData(n_times = n_times,
                                 times_utc = times_utc,
                                 airspeed = numpy.ma.masked_values(airspeed_list, missing_airspeed),
                                 latitude = numpy.ma.masked_values(lat_list, missing_lat),
                                 longitude = numpy.ma.masked_values(lon_list, missing_lon),
                                 altitude = numpy.ma.masked_values(alt_list, missing_alt))

    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure time is strictly increasing
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        n_times = times_utc.shape[0]
        airspeed = numpy.ma.concatenate([getattr(x, 'airspeed') for x in data_tuple_list])
        latitude = numpy.ma.concatenate([getattr(x, 'latitude') for x in data_tuple_list])
        longitude = numpy.ma.concatenate([getattr(x, 'longitude') for x in data_tuple_list])
        altitude = numpy.ma.concatenate([getattr(x, 'altitude') for x in data_tuple_list])
        data_tuple = CitationNavData(n_times = n_times,
                                     times_utc = times_utc,
                                     airspeed = airspeed,
                                     latitude = latitude,
                                     longitude = longitude,
                                     altitude = altitude)
    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.fd12p._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0
    return data_tuple


def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)
    return data_tuple


def subset_loc(data_tuple, lat_start, lat_end, lon_start, lon_end):

    mask_lat = numpy.logical_and(numpy.less_equal(lat_start, data_tuple.latitude), numpy.less_equal(data_tuple.latitude, lat_end))
    mask_lon = numpy.logical_and(numpy.less_equal(lon_start, data_tuple.longitude), numpy.less_equal(data_tuple.longitude, lon_end))
    mask = numpy.logical_and(mask_lat, mask_lon)
    n_times = numpy.sum(mask)

    data_tuple_subset = CitationNavData(n_times = n_times,
                                        times_utc = data_tuple.times_utc[mask],
                                        airspeed = data_tuple.airspeed[mask],
                                        latitude = data_tuple.latitude[mask],
                                        longitude = data_tuple.longitude[mask],
                                        altitude = data_tuple.altitude[mask])
    return data_tuple_subset


def test():
    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/Navigation/nav_2012_01_27_01_55_33.gcpex',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/Navigation/nav_2012_01_28_15_49_01.gcpex',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/Navigation/nav_2012_01_30_22_40_15.gcpex']
    data = load(fpaths)

    for i_time in range(data.n_times):
        print(data.times_utc[i_time], data.altitude[i_time], data.airspeed[i_time])


if __name__ == '__main__':
    test()

