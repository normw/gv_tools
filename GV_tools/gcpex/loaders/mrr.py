#!/usr/bin/env python3


"""Structure and methods for Micro Rain Radar data
"""

#--------------------------------------------------------------------------
# Structure and methods for Micro Rain Radar data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import sys

import numpy

import collections
import netCDF4
from ...utilities import tools
from . import diagnostics


RadarData = collections.namedtuple('RadarData', ['n_times',
                                                 'n_bins',
                                                 'times_utc', 
                                                 'heights', 
                                                 'refl', 
                                                 'v_doppler'])


def _load_single(fpath):

    data_tuple = None
    dset = netCDF4.Dataset(fpath, 'r')
    try:
       times_utc = tools.nc4_unixtime_to_datetime64(dset.variables['time'])
    except tools.TimeDimensionError:
       print(fpath)
       assert 0
    n_bins = len(dset.dimensions['range'])
    n_times = len(dset.dimensions['time'])
    heights = tools.nc4_to_ma(dset.variables['height'])
    refl = tools.nc4_to_ma(dset.variables['Ze'])
    v_doppler = tools.nc4_to_ma(dset.variables['W'])

    data_tuple = RadarData(n_times = n_times,
                           n_bins = n_bins,
                           times_utc = times_utc,
                           heights = heights,
                           refl = refl,
                           v_doppler = v_doppler)
    dset.close()
    return data_tuple


def _clean_overlaps(data_tuple_list_orig):
    
    data_tuple_list = []

    for i_tuple, data_tuple_current in enumerate(data_tuple_list_orig[:-1]):
        data_tuple_next = data_tuple_list_orig[i_tuple+1]
        times_utc_current = data_tuple_current.times_utc
        times_utc_next = data_tuple_next.times_utc

        overlap = numpy.in1d(times_utc_current, times_utc_next)

        if numpy.any(overlap):
            unique = numpy.logical_not(overlap)
            n_times = numpy.sum(unique)
            n_bins = data_tuple_current.n_bins
            times_utc = data_tuple_current.times_utc[unique]
            heights = data_tuple_current.heights[unique, :]
            refl = data_tuple_current.refl[unique, :]
            v_doppler = data_tuple_current.v_doppler[unique, :]
            data_tuple = RadarData(n_times = n_times,
                                   n_bins = n_bins,
                                   times_utc = times_utc,
                                   heights = heights,
                                   refl = refl,
                                   v_doppler = v_doppler)
            data_tuple_list.append(data_tuple)
        else:
            data_tuple_list.append(data_tuple_current)

    #Append the last data tuple in the list
    data_tuple_list.append(data_tuple_list_orig[-1])

    return data_tuple_list   
        
        

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list_tmp = []
    for fpath in fpaths:
        data_tuple_list_tmp.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list_tmp])

    #Make sure that time is strictly increasing
    if not numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        #Try to correct and update times_utc
        data_tuple_list = _clean_overlaps(data_tuple_list_tmp)
        times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
        #Check cleaning of overlaps
        if not numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
            #Bail out
            sys.stderr.write('times_utc values not strictly increasing in loaders.mrr._concatenate()\n')
            diagnostics.sequential_time_error_handler(data_tuple_list)
            assert 0
    else:
        data_tuple_list = data_tuple_list_tmp

    #Create the concatenated data tuple
    #It's not certain that all the 2D arrays use the same number of range bins, but
    #lets try that
    try:
        heights = numpy.ma.concatenate([getattr(x, 'heights') for x in data_tuple_list])
        refl = numpy.ma.concatenate([getattr(x, 'refl') for x in data_tuple_list])
        v_doppler = numpy.ma.concatenate([getattr(x, 'v_doppler') for x in data_tuple_list])
    except ValueError as e:
        sys.stderr.write('Error concatenating MRR datasets in loaders.mrr._concatenate()\n')
        assert 0
    n_times = times_utc.shape[0]
    n_bins = refl.shape[1]
    data_tuple = RadarData(n_times = n_times,
                           n_bins = n_bins,
                           times_utc = times_utc,
                           heights = heights,
                           refl = refl,
                           v_doppler = v_doppler)

    return data_tuple


def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple


def test():
    
    import netCDF4
    import datetime
    import numpy

    print("Running local test")

    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/MRR/CARE_EC/NetCDF/MRR_20120128_GCPEx_CARE-EC_snow.nc',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/MRR/CARE_EC/NetCDF/MRR_20120129_GCPEx_CARE-EC_snow.nc']


    data = load(fpaths)
    dset = netCDF4.Dataset(fpaths[0], 'r')
    print('reflectivity is MA:', isinstance(data.refl, numpy.ma.MaskedArray))
    print('times_utc is MA:', isinstance(data.times_utc, numpy.ma.MaskedArray))
    (n_times, n_bins) =  data.refl.shape
    print(data.refl.shape)
    n_times = data.n_times
    unixtime_var = data.times_utc
    mask_refl = data.refl.mask
    for i_time in range(n_times):
        print(unixtime_var[i_time], data.times_utc[i_time], data.refl[i_time, 5], data.v_doppler[i_time, 5], mask_refl[i_time, 5])

if __name__ == '__main__':
    test()


