#!/usr/bin/env python3


"""Structure and methods for meteorology tower data
"""

#--------------------------------------------------------------------------
# Structure and methods for meteorology tower data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import os
import sys
import numpy
import collections
from ...utilities import tools
from . import diagnostics

MetData = collections.namedtuple('MetData', ['n_times',
                                             'times_utc',
                                             'u_2m',
                                             'u_10m',
                                             'phi_10m',
                                             'T',
                                             'P',
                                             'RH',
                                             'T_corr',
                                             'P_corr'])


_MISSING_STRING = ''

def _date_from_filename(fpath):
    """ Extract the date from a Met Tower filename """

    directory, fname = os.path.split(fpath)
    parts = fname.split('_')
    year = int(parts[2][0:4])
    month = int(parts[2][4:6])
    day = int(parts[2][6:8])
    return (year, month, day)

def _parse_line(line):
    """ Parse a line of Met Tower data """

    N_elements_expected = 28

    elements = line.split(',')
    if len(elements) == N_elements_expected:
        return elements
    else:
        return None


def _load_single(fpath):

    data_tuple = None

    datetime_list = []
    u_2m_list = []
    u_10m_list = []
    phi_10m_list = []
    T_list = []
    P_list = []
    RH_list = []
    T_corr_list = []
    P_corr_list = []

    #Get the date from the fpath
    (sample_year, sample_month, sample_day) = _date_from_filename(fpath)
    base_time = numpy.datetime64('%4d-%02d-%02d' %(sample_year, sample_month, sample_day))

    with open(fpath, 'r') as f_in:
        lines = f_in.readlines()

    for line in lines:
        elements = _parse_line(line)
        if elements is not None:
            hms_element = elements[0]
            if hms_element not in _MISSING_STRING:
                try:
                    timedelta = numpy.timedelta64(tools.hms_to_seconds(hms_element), 's')
                except ValueError:
                    #If no valid timedelta, continue to next line of data
                    continue
                datetime_list.append(base_time + timedelta)
                u_2m_list.append(elements[10])
                u_10m_list.append(elements[4])
                phi_10m_list.append(elements[5])
                T_list.append(elements[16])
                P_list.append(elements[22])
                RH_list.append(elements[18])
                T_corr_list.append(elements[26])
                P_corr_list.append(elements[27])
            else:
                #If there's not a valid hms string to convert to a timedelta,
                #continue to next line of data
                continue

    datetime_array = numpy.array(datetime_list, dtype='datetime64')
    u_2m_array = tools.list_to_MA_float(u_2m_list)
    u_10m_array = tools.list_to_MA_float(u_10m_list)
    phi_10m_array = tools.list_to_MA_float(phi_10m_list)
    T_array = tools.list_to_MA_float(T_list)
    P_array = tools.list_to_MA_float(P_list)
    RH_array = tools.list_to_MA_float(RH_list)
    T_corr_array = tools.list_to_MA_float(T_corr_list)
    P_corr_array = tools.list_to_MA_float(P_corr_list)
    n_times = datetime_array.shape[0]

    data_tuple = MetData(n_times = n_times,
                         times_utc = datetime_array,
                         u_2m = u_2m_array,
                         u_10m = u_10m_array,
                         phi_10m = phi_10m_array,
                         T = T_array,
                         P = P_array,
                         RH = RH_array,
                         T_corr = T_corr_array,
                         P_corr = P_corr_array)

    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
       data_tuple_list.append(_load_single(fpath))

    datetime_array = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure time is strictly increasing
    if numpy.all(numpy.diff(datetime_array) > numpy.timedelta64(0, 's')):
        u_2m_array = numpy.ma.concatenate([getattr(x, 'u_2m') for x in data_tuple_list])
        u_10m_array = numpy.ma.concatenate([getattr(x, 'u_10m') for x in data_tuple_list])
        phi_10m_array = numpy.ma.concatenate([getattr(x, 'phi_10m') for x in data_tuple_list])
        T_array = numpy.ma.concatenate([getattr(x, 'T') for x in data_tuple_list])
        P_array = numpy.ma.concatenate([getattr(x, 'P') for x in data_tuple_list])
        RH_array = numpy.ma.concatenate([getattr(x, 'RH') for x in data_tuple_list])
        T_corr_array = numpy.ma.concatenate([getattr(x, 'T_corr') for x in data_tuple_list])
        P_corr_array = numpy.ma.concatenate([getattr(x, 'P_corr') for x in data_tuple_list])
        n_times = datetime_array.shape[0]
        data_tuple = MetData(n_times = n_times,
                             times_utc = datetime_array,
                             u_2m = u_2m_array,
                             u_10m = u_10m_array,
                             phi_10m = phi_10m_array,
                             T = T_array,
                             P = P_array,
                             RH = RH_array,
                             T_corr = T_corr_array,
                             P_corr = P_corr_array)
    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.met._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0
    
    return data_tuple


def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple


def test():

    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Met_tower/Huronia/gcpex_met_20120129_EC_HURONIA.txt',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Met_tower/Huronia/gcpex_met_20120130_EC_HURONIA.txt']
    data = load(fpaths)
    n_times = data.times_utc.shape[0]
    for i_time in range(n_times):
        print(data.times_utc[i_time], data.T[i_time], data.RH[i_time], data.u_2m[i_time])


if __name__ == '__main__':
    test()
