#!/usr/bin/env python3


"""Structure and methods for Pluvio data
"""

#--------------------------------------------------------------------------
# Structure and methods for Pluvio data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


"""Load Pluvio data structure

Copyright (c) 2017 Norman Wood.  All rights reserved.

This file is part of GV_tools.  It is subject to the license terms of
the BSD-3-Clause license, the full text of which is available in the
LICENSE file found in the top-level directory of the GV_tools
distribution.  No part of GV_tools, including this file, may be copied,
modified, propagated, or distributed except according to the terms
contained in the LICENSE file.


This is for use with the files in
/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Pluvio_alt,
specifically the 'all' files, e.g., pluvioxxx_all.care, pluvioxxx_all.huronia, etc.
See the README.txt file for information about the source.
We use the 'Total NRT' output as the precip accumulation during the time period.
"""

import sys
import numpy
import collections
from ...utilities import tools
from . import diagnostics

PluvioData = collections.namedtuple('PluvioData', ['n_times', 
                                                   'times_utc', 
                                                   'accumulation'])



def _adjust_doy(year, doy):
    #Kludge for stupid pluvio junk
    leap_years = [2004, 2008, 2012, 2016, 2020, 2024]
    if doy == 0:
        year = year - 1
    if year in leap_years:
        doy = 366
    else:
        doy = 365
    return year, doy

def _load_single(fpath):

    data_tuple = None

    datetime_list = []
    accumulation_list = []
    
    with open(fpath, 'r') as f_in:
        lines = f_in.readlines()

    for line in lines:
        elements = line.split()
        year = int(elements[0])
        doy = int(elements[1])
        hour = int(elements[2])
        minute = int(elements[3])
        seconds = 0.
        if doy == 0:
           year, doy = _adjust_doy(year, doy)
        
        year, month, day = tools.day_of_year_to_month_day(doy, year)
        datetime_str = '%4d-%02d-%02dT%02d:%02d:%09.6f' %(year, month, day, hour, minute, seconds)

        try:
            datetime_value = numpy.datetime64(datetime_str)
        except ValueError:
            #Not a valid time value, skip to next line
            continue

        try:
            accumulation_value = float(elements[7])
        except ValueError:
            #Can't be converted to a float value, skip to next line
            continue
        
        datetime_list.append(datetime_value)
        accumulation_list.append(accumulation_value)

    n_times = len(datetime_list)
    accumulation_array = numpy.array(accumulation_list, dtype=float)
    datetime_array = numpy.array(datetime_list, dtype=numpy.datetime64)

    #Check for resets in the accumulation (goes from positive to zero in one timestep)
    reset_index_list = []
    for idx in range(n_times-1):
        if accumulation_array[idx] > accumulation_array[idx+1] and numpy.isclose(accumulation_array[idx+1], 0.):
             reset_index_list.append(idx)
             print('Reset event at %s' %(datetime_array[idx],))
             print(accumulation_array[idx], accumulation_array[idx+1])

    for idx in reversed(reset_index_list):
        accumulation_array[idx+1:] += accumulation_array[idx]

    data_tuple = PluvioData(n_times = n_times,
                            times_utc = datetime_array,
                            accumulation = accumulation_array)

    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        accumulation = numpy.concatenate([getattr(x, 'accumulation') for x in data_tuple_list])
        n_times = times_utc.shape[0]

        data_tuple = PluvioData(n_times = n_times,
                                times_utc = times_utc,
                                accumulation = accumulation)
    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.pluvio._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0
        
    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple


def test():
    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Pluvio_alt/pluvio200_all.care',]
    data = load(fpaths)

    for i_time in range(data.n_times):
        print(data.times_utc[i_time], data.accumulation[i_time])


if __name__ == '__main__':
    test()
