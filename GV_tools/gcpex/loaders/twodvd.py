#!/usr/bin/env python3



"""Structure and methods for 2D Video Disdrometer data
"""

#--------------------------------------------------------------------------
# Structure and methods for 2D Video Disdrometer data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


# Tools for ingesting 2DVD data, specifically the rematch data created
# by Huang at Colorado State
# 
# This is for use with the files in
# /boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/2DVD,
# specifically the 'flakes_rematch.txt' files.
# See the README.txt file for information about the source.
# 
# The fields in the ascii rematch data are (see email from Guo-Jong Huang on
# 28 Jan 2009):
# 
#  0:  Scan line of the day
#  1:  Apparent diameter, mm
#  2:  Fall speed, m/s
#  3:  Sample area (cross-sectional area that is sampled), mm^2
#  4:  Area shadowed in camera A, mm^2
#  5:  Area shadowed in camera B, mm^2
#  6:  Height in camera A, mm
#  7:  Rectangular width in camera A, mm
#  8:  Height in camera B, mm
#  9:  Rectangular width in camera B, mm
# 
# When the data are loaded, the sample area is converted from mm^2 to m^2.
# Other data are left in their original units.

import sys
import os
import collections
import numpy

from ...utilities import tools
from . import diagnostics


TwoDVDData = collections.namedtuple('TwoDVDData', ['n_times', 
                                                   'times_utc', 
                                                   'D_major',
                                                   'fallspeed', 
                                                   'sampling_area'])

#Scans per second
_linescan_freqs = {'sn25': 55172.,
                   'sn35': 55272.,
                   'sn36': 55272.,
                   'sn37': 55272.,
                   'sn38': 55272.}


def _scanline_to_seconds(scan_line_number, linescan_freq):
    seconds = scan_line_number/linescan_freq
    return seconds
    
def _load_single(fpath):

    data_tuple = None

    #Get year, month, day from filename
    directory, fname = os.path.split(fpath)
    parts = fname.split('_')
    year = int(parts[6][0:4])
    month = int(parts[6][4:6])
    day = int(parts[6][6:8])

    #Get serial number to determine line scan frequency
    serial_number_string = parts[1]
    linescan_freq = _linescan_freqs[serial_number_string]


    datetime_list = []
    fallspeed_list = []
    width_list = []
    sampling_area_list = []
    
    with open(fpath, 'r') as f_in:
        lines = f_in.readlines()

    for line in lines:
        elements = line.split()
        scan_line_number = int(elements[0])
        fallspeed = float(elements[2])
        sampling_area = float(elements[3])
        shadow_area_A = float(elements[4])
        shadow_area_B = float(elements[5])
        height_A = float(elements[6])
        particle_width_A = float(elements[7])
        height_B = float(elements[8])
        particle_width_B = float(elements[9])
        
        seconds = _scanline_to_seconds(scan_line_number, linescan_freq)
        hour, minute, seconds = tools.seconds_to_HMS(seconds)

        datetime_str = '%4d-%02d-%02dT%02d:%02d:%09.6f' %(year, month, day, hour, minute, seconds)
        try:
            datetime_value = numpy.datetime64(datetime_str)
        except ValueError:
            #Can't get a valid time stamp, skip to next line
            continue

        datetime_list.append(datetime_value)
        fallspeed_list.append(fallspeed)
        width_list.append(max(particle_width_A, particle_width_B))
        sampling_area_list.append(sampling_area)
        

    n_times = len(datetime_list)
    fallspeed_array = numpy.array(fallspeed_list)
    width_array = numpy.array(width_list)
    datetime_array = numpy.array(datetime_list, dtype=numpy.datetime64)
    sampling_area_array = numpy.array(sampling_area_list)*1.0e-6   #Convert to m^2

    data_tuple = TwoDVDData(n_times = n_times,
                            times_utc = datetime_array,
                            D_major = width_array,
                            fallspeed = fallspeed_array,
                            sampling_area = sampling_area_array)

    return data_tuple


def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    datetime_array = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #For the 2DVD, the time sampling is so rapid, it appears that sometimes the time
    #stamps can be repeated on subsequent samples.  Allow greater or equal.
    #Make sure time is strictly increasing
    if numpy.all(numpy.diff(datetime_array) >= numpy.timedelta64(0, 's')):
        fallspeed_array = numpy.concatenate([getattr(x, 'fallspeed') for x in data_tuple_list])
        width_array = numpy.concatenate([getattr(x, 'D_major') for x in data_tuple_list])
        sampling_area_array = numpy.concatenate([getattr(x, 'sampling_area') for x in data_tuple_list])
        n_times = datetime_array.shape[0]

        data_tuple = TwoDVDData(n_times = n_times,
                                times_utc = datetime_array,
                                D_major = width_array,
                                fallspeed = fallspeed_array,
                                sampling_area = sampling_area_array)

    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.twodvd._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0

    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple



def test():
    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/2DVD/CARE/2dvd_sn37_gcpex_CARE_N441359.45_W0794650.11_20120130.flakes_rematch.txt',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/2DVD/CARE/2dvd_sn37_gcpex_CARE_N441359.45_W0794650.11_20120211.flakes_rematch.txt']
    data = load(fpaths)

    for i_time in range(data.n_times):
        print(data.times_utc[i_time], data.D_major[i_time], data.fallspeed[i_time], data.sampling_area[i_time])

if __name__ == "__main__":
    test()
