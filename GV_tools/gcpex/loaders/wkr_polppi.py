#!/usr/bin/env python3



"""Structure and methods for King City radar POLPPI data
"""

#--------------------------------------------------------------------------
# Structure and methods for King City radar POLPPI data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import sys
import os
import collections
import numpy
import pyart

from . import diagnostics

POLPPIData = collections.namedtuple('POLPPIData', ['n_times', 
                                                   'times_utc',
                                                   'lat', 
                                                   'lon',
                                                   'alt',
                                                   'refl'])


def _load_single(fpath):

    #For POLPPI data, we take arrays that are 2D(time, radar bin) and
    #flatten them.  This will simplify the subsetting later on, but it
    #requires some special handling to create a time vector of the same shape.

    data_tuple = None
    try:
        radar_data = pyart.io.read_sigmet(fpath, time_ordered='sequential')
    except IOError as err:
        print('I/O error({0}: {1}'.format(err.errno, err.strerror))
        print('For file %s' %(fpath,))
        print('Skipping')
        return data_tuple

    #Get the base time from the time units
    time_unit_string = radar_data.time['units']  #Like "seconds since 2012-01-28T23:43:36Z"
    parts = time_unit_string.split()
    iso_base_time_string = parts[2][0:19]        #Take only the first 19 characters. Trim
                                                 #off time zone info to avoid numpy deprecation
                                                 #warning.
    base_time = numpy.datetime64(iso_base_time_string)

    time_offset_seconds = radar_data.time['data'].astype('int')

    time_utc = numpy.array([base_time + numpy.timedelta64(x, 's') for x in time_offset_seconds])

    #Do row-major flattening
    lat = radar_data.gate_latitude['data'].flatten()
    lon = radar_data.gate_longitude['data'].flatten()
    alt = radar_data.gate_altitude['data'].flatten()
    refl = radar_data.fields['reflectivity']['data'].flatten()

    #Now reshape the time_utc vector appropriately
    (N_rays, N_bins) = radar_data.fields['reflectivity']['data'].shape
    #time_utc_flat = numpy.empty((N_rays*N_bins,), dtype=type(time_utc[0]))
    time_utc_flat = numpy.empty((N_rays*N_bins,), dtype='datetime64[s]')
    for idx_ray in range(N_rays):
        idx_flat_start = idx_ray*N_bins
        idx_flat_end = idx_flat_start + N_bins + 1
        time_utc_flat[idx_flat_start:idx_flat_end] = time_utc[idx_ray]

    n_times =  N_rays*N_bins
    data_fields = [n_times, time_utc_flat, lat, lon, alt, refl]

    #Create the POLPPIData namedtuple
    data_tuple = POLPPIData(*data_fields)

    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        tmp_data_tuple = _load_single(fpath)
        if tmp_data_tuple is not None:
            data_tuple_list.append(tmp_data_tuple)

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #The time resolution in these polppi iri files is seconds, and several sequential
    #data elements may have the same time stamp.  Just check that delta times are
    #greater than or equal to zero (increasing), rather than strictly
    #increasing.
    if numpy.all(numpy.diff(times_utc) >= numpy.timedelta64(0, 's')):
        n_times = times_utc.shape[0]
        lat = numpy.ma.concatenate([getattr(x, 'lat') for x in data_tuple_list])
        lon = numpy.ma.concatenate([getattr(x, 'lon') for x in data_tuple_list])
        alt = numpy.ma.concatenate([getattr(x, 'alt') for x in data_tuple_list])
        refl = numpy.ma.concatenate([getattr(x, 'refl') for x in data_tuple_list])

        data_fields = [n_times, times_utc, lat, lon, alt, refl]
        data_tuple = POLPPIData(*data_fields)

    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.wkr_polppi._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0

    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple


#Helper routine to subset data
def subset_loc(data, lat_start, lat_end, lon_start, lon_end):

    mask_lat = numpy.logical_and(numpy.less_equal(lat_start, data.lat), numpy.less_equal(data.lat, lat_end))
    mask_lon = numpy.logical_and(numpy.less_equal(lon_start, data.lon), numpy.less_equal(data.lon, lon_end))
    mask = numpy.logical_and(mask_lat, mask_lon)

    data_subset = None
    if numpy.any(mask):
        n_times = numpy.sum(mask)
        data_subset = POLPPIData(n_times = n_times,
                                 times_utc = data.times_utc[mask],
                                 lat = data.lat[mask],
                                 lon = data.lon[mask],
                                 alt = data.alt[mask],
                                 refl = data.refl[mask])
    return data_subset


def test():
    from ...utilities import tools
    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/WKR/POLPPI/Data/20120128/gcpex_wkr_201201281720_polppi.iri',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/WKR/POLPPI/Data/20120128/gcpex_wkr_201201281730_polppi.iri',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/WKR/POLPPI/Data/20120128/gcpex_wkr_201201281740_polppi.iri']

    data = load(fpaths)
    
    #~CARE
    subset = subset_loc(data, 44.21, 44.27, -79.82, -79.74)

    frac_days = tools.datetime64_to_frac_day(subset.times_utc)
    frac_hrs = frac_days*24.
    for i_time, frac_hr in enumerate(frac_hrs):
        print(5*'%12.6f ' %(frac_hr, subset.lat[i_time], subset.lon[i_time], subset.alt[i_time], subset.refl[i_time]))


if __name__ == "__main__":
    test()
