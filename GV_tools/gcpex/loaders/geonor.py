#!/usr/bin/env python3

"""Load Geonor data structure

Copyright (c) 2017 Norman Wood.  All rights reserved.

This file is part of GV_tools.  It is subject to the license terms of
the BSD-3-Clause license, the full text of which is available in the
LICENSE file found in the top-level directory of the GV_tools
distribution.  No part of GV_tools, including this file, may be copied,
modified, propagated, or distributed except according to the terms
contained in the LICENSE file.
"""


import numpy
import datetime
import collections
from . import diagnostics

GeonorData = collections.namedtuple('GeonorData', ['n_times',
                                                   'times_utc',
                                                   'accumulation'])


_MISSING_ACCUMULATION = -999.

def _load_single(fpath):

    data_tuple = None

    datetime_list = []
    accumulation_list = []

    with open(fpath, 'r') as f_in:
        lines = f_in.readlines()

    for line in lines:
        elements = line.split()

        try:
            year_str, month_str, day_str = elements[1][0:4], elements[1][4:6], elements[1][6:8]
            hour_str, minute_str = elements[2][0:2], elements[2][2:4]
            second_str = '00'
            #datetime_str = '%s %s %s %s:%s' %(year_str, month_str, day_str, hour_str, minute_str)
            #datetime_value = numpy.datetime64(datetime.datetime.strptime(datetime_str, '%Y %m %d %H:%M'))
            datetime_str = '%s-%s-%sT%s:%s:%s' %(year_str, month_str, day_str, hour_str, minute_str, second_str)
            datetime_value = numpy.datetime64(datetime_str)
        except (ValueError, IndexError):
            #No valid timestamp for this data, skip to next line
            continue

        #Otherwise, try to get the accumulation
        try:
            accumulation_value = float(elements[9])
            if accumulation_value < 0.:
                accumulation_value = _MISSING_ACCUMULATION
        except ValueError:
            accumulation_value = _MISSING_ACCUMULATION

        datetime_list.append(datetime_value)
        accumulation_list.append(accumulation_value)

    datetime_array = numpy.array(datetime_list, dtype='datetime64')
    accumulation_array = numpy.ma.masked_values(accumulation_list, _MISSING_ACCUMULATION)
    n_times = datetime_array.shape[0]

    data_tuple  = GeonorData(n_times = n_times,
                             times_utc = datetime_array,
                             accumulation = accumulation_array)
    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure time is strictly increasing
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        accumulation = numpy.ma.concatenate([getattr(x, 'accumulation') for x in data_tuple_list])
        n_times = times_utc.shape[0]
        data_tuple = GeonorData(n_times = n_times,
                                times_utc = times_utc,
                                accumulation = accumulation)
    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.geonor._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0

    return data_tuple


def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)
    return data_tuple


def build_accumulations(start_datetime, fpaths):

    data = load(fpaths)    
    mask = numpy.greater(data.times_utc, start_datetime)

    total_accumulations = numpy.cumsum(accumulations[mask])
    total_accumulations -= total_accumulations[0]

    return(datetimes[mask], total_accumulations)


def test():
    #List of file paths in temporal order

    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/USCRN_Geonor/CRNS0101-05-2011-ON_Egbert_1_W.txt',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/USCRN_Geonor/CRNS0101-05-2012-ON_Egbert_1_W.txt']

    data = load(fpaths)    
    for i_time, time_utc in enumerate(data.times_utc):
        print(data.times_utc[i_time], data.accumulation[i_time])

if __name__ == "__main__":
    test()
