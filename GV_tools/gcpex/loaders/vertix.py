#!/usr/bin/env python3


"""Structure and methods for VertiX radar data
"""

#--------------------------------------------------------------------------
# Structure and methods for VertiX radar data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import sys
import numpy
import collections
import netCDF4

from ...utilities import tools
from . import diagnostics

RadarData = collections.namedtuple('RadarData', ['n_times', 
                                                 'n_bins', 
                                                 'times_utc', 
                                                 'heights', 
                                                 'refl', 
                                                 'v_doppler'])




def _load_single(fpath):

    data_tuple = None

    dset = netCDF4.Dataset(fpath, 'r')
    times_utc = tools.nc4_unixtime_to_datetime64(dset.variables['base_time'])
    n_bins = len(dset.dimensions['maxCells'])
    n_times = len(dset.dimensions['time'])
    #Convert heights to meters
    heights = tools.nc4_to_ma(dset.variables['Range'])*1000.
    refl = tools.nc4_to_ma(dset.variables['DBZ'])
    v_doppler = tools.nc4_to_ma(dset.variables['DV'])

    data_tuple = RadarData(n_times = n_times,
                           n_bins = n_bins,
                           times_utc = times_utc,
                           heights = heights,
                           refl = refl,
                           v_doppler = v_doppler)
    dset.close()
    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure that time is strictly increasing
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        #It's not certain that all the 2D arrays use the same number of range bins, but
        #lets try that
        try:
            heights = numpy.ma.concatenate([getattr(x, 'heights') for x in data_tuple_list])
            refl = numpy.ma.concatenate([getattr(x, 'refl') for x in data_tuple_list])
            v_doppler = numpy.ma.concatenate([getattr(x, 'v_doppler') for x in data_tuple_list])
        except ValueError as e:
            sys.stderr.write('Error concatenating VertiX datasets in loaders.vertix._concatenate()\n')
            assert 0
        n_times = times_utc.shape[0]
        n_bins = refl.shape[1]
        data_tuple = RadarData(n_times = n_times,
                               n_bins = n_bins,
                               times_utc = times_utc,
                               heights = heights,
                               refl = refl,
                               v_doppler = v_doppler)
    else:
       sys.stderr.write('times_utc values not strictly increasing in loaders.vertix._concatenate()\n')
       diagnostics.sequential_time_error_handler(data_tuple_list)
       assert 0

    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple



def test():

    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/VertiX/vertix_gcpex_20120128.nc',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/VertiX/vertix_gcpex_20120129.nc']

    data = load(fpaths)

    for i_time in range(data.n_times):
        print(data.times_utc[i_time], data.refl[i_time, 15], data.v_doppler[i_time, 15])

if __name__ == "__main__":
    test()
