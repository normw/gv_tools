#!/usr/bin/env python3

import numpy
import collections

_MISSING_FLOAT = -999.
_MISSING_INT = -999


#File properties
_N_DATA_ELEMENTS = 45
_N_MISSING_VALUE_FLAG_ELEMENTS = 44  #The last column doesn't have a value set to flag missing values, it is a time stamp
_LINENUM_BASE_DATE = 6
_LINENUM_INVALID_FLAGS = 11
_LINENUM_DATA_START = 62


CitationMicrophysData = collections.namedtuple('CitationMicrophysData', ['n_times',
                                                                         'times_utc',
                                                                         'latitude',
                                                                         'longitude',
                                                                         'altitude',
                                                                         'T',
                                                                         'P',
                                                                         'lwc_King',
                                                                         'twc_Nevz',
                                                                         'lwc_Nevz',
                                                                         'lwc_CDP'])


def _extract_missing_flag_values(line):
    data = None
    elements = line.split()
    if len(elements) == _N_MISSING_VALUE_FLAG_ELEMENTS:
        #Proper data line
        lat = float(elements[17])
        lon = float(elements[18])
        alt = float(elements[19])
        temp = float(elements[0])
        pres = float(elements[8])
        lwc_King = float(elements[29])
        twc_Nevz = float(elements[30])
        lwc_Nevz = float(elements[31])
        lwc_CDP = float(elements[33])

        data = (lat, lon, alt, temp, pres, lwc_King, twc_Nevz, lwc_Nevz, lwc_CDP)

    return data

def _extract_data(line):
    data = None
    elements = line.split()
    if len(elements) == _N_DATA_ELEMENTS:
        #Proper data line
        time_s = float(elements[0])
        lat = float(elements[18])
        lon = float(elements[19])
        alt = float(elements[20])
        temp = float(elements[1])
        pres = float(elements[9])
        lwc_King = float(elements[30])
        twc_Nevz = float(elements[31])
        lwc_Nevz = float(elements[32])
        lwc_CDP = float(elements[34])
        data = (time_s, lat, lon, alt, temp, pres, lwc_King, twc_Nevz, lwc_Nevz, lwc_CDP)

    return data


def _load_single(fpath):

    data_tuple = None

    time_list = []
    lat_list = []
    lon_list = []
    alt_list = []
    temp_list = []
    pres_list = []
    lwc_King_list = []
    twc_Nevz_list = []
    lwc_Nevz_list = []
    lwc_CDP_list = []

    with open(fpath, 'r') as f_in:
        lines = f_in.readlines()

    #Get the date of data collections, line 6
    #and create a corresponding datetime64 object
    elements = lines[_LINENUM_BASE_DATE].split()
    year = int(elements[0])
    month = int(elements[1])
    day = int(elements[2])

    time_base = numpy.datetime64('%4d-%02d-%02dT00:00:00' %(year, month, day))

    #Get the list of values that indicate missing data
    missing_flag_values =  _extract_missing_flag_values(lines[_LINENUM_INVALID_FLAGS])

    (missing_lat,
     missing_lon,
     missing_alt,
     missing_temp,
     missing_pres,
     missing_lwc_King,
     missing_twc_Nevz,
     missing_lwc_Nevz,
     missing_lwc_CDP) = missing_flag_values

    for line in lines[_LINENUM_DATA_START:]:
        data = _extract_data(line)
        if data is not None:
            (time_s, lat, lon, alt, temp, pres, lwc_King, twc_Nevz, lwc_Nevz, lwc_CDP) = data
            time_list.append(time_s)
            lat_list.append(lat)
            lon_list.append(lon)
            alt_list.append(alt)
            temp_list.append(temp)
            pres_list.append(pres)
            lwc_King_list.append(lwc_King)
            twc_Nevz_list.append(twc_Nevz)
            lwc_Nevz_list.append(lwc_Nevz)
            lwc_CDP_list.append(lwc_CDP)


    n_times = len(time_list)

    offset_frac, offset_int = numpy.modf(time_list)
    offset_parts = zip(offset_int, offset_frac)

    time_deltas = [numpy.timedelta64(int(x[0]),'s') + numpy.timedelta64(int(x[1]*1000.), 'ms') for x in offset_parts]
    times_utc = time_base + time_deltas

    latitude = numpy.ma.masked_values(lat_list, missing_lat)
    longitude = numpy.ma.masked_values(lon_list, missing_lon)
    altitude = numpy.ma.masked_values(alt_list, missing_alt)
    T = numpy.ma.masked_values(temp_list, missing_temp) + 273.15
    P = numpy.ma.masked_values(pres_list, missing_pres)
    lwc_King = numpy.ma.masked_values(lwc_King_list, missing_lwc_King)
    twc_Nevz = numpy.ma.masked_values(twc_Nevz_list, missing_twc_Nevz)
    lwc_Nevz = numpy.ma.masked_values(lwc_Nevz_list, missing_lwc_Nevz)
    lwc_CDP = numpy.ma.masked_values(lwc_CDP_list, missing_lwc_CDP)

    data_tuple = CitationMicrophysData(n_times = n_times,
                                       times_utc = times_utc,
                                       latitude = latitude,
                                       longitude = longitude,
                                       altitude = altitude,
                                       T = T, 
                                       P = P,
                                       lwc_King = lwc_King,
                                       twc_Nevz = twc_Nevz,
                                       lwc_Nevz = lwc_Nevz,
                                       lwc_CDP = lwc_CDP)

    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure time is strictly increasing
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        n_times = times_utc.shape[0]
        latitude = numpy.ma.concatenate([getattr(x, 'latitude') for x in data_tuple_list])
        longitude = numpy.ma.concatenate([getattr(x, 'longitude') for x in data_tuple_list])
        altitude = numpy.ma.concatenate([getattr(x, 'altitude') for x in data_tuple_list])
        T = numpy.ma.concatenate([getattr(x, 'T') for x in data_tuple_list])
        P = numpy.ma.concatenate([getattr(x, 'P') for x in data_tuple_list])
        lwc_King = numpy.ma.concatenate([getattr(x, 'lwc_King') for x in data_tuple_list])
        twc_Nevz = numpy.ma.concatenate([getattr(x, 'twc_Nevz') for x in data_tuple_list])
        lwc_Nevz = numpy.ma.concatenate([getattr(x, 'lwc_Nevz') for x in data_tuple_list])
        lwc_CDP = numpy.ma.concatenate([getattr(x, 'lwc_CDP') for x in data_tuple_list])
        
        data_tuple = CitationMicrophysData(n_times = n_times,
                                           times_utc = times_utc,
                                           latitude = latitude,
                                           longitude = longitude,
                                           altitude = altitude,
                                           T = T, 
                                           P = P,
                                           lwc_King = lwc_King,
                                           twc_Nevz = twc_Nevz,
                                           lwc_Nevz = lwc_Nevz,
                                           lwc_CDP = lwc_CDP)

    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.fd12p._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0
    return data_tuple


def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)
    return data_tuple


def subset_loc(data_tuple, lat_start, lat_end, lon_start, lon_end):

    mask_lat = numpy.logical_and(numpy.less_equal(lat_start, data_tuple.latitude), numpy.less_equal(data_tuple.latitude, lat_end))
    mask_lon = numpy.logical_and(numpy.less_equal(lon_start, data_tuple.longitude), numpy.less_equal(data_tuple.longitude, lon_end))
    mask = numpy.logical_and(mask_lat, mask_lon)
    n_times = numpy.sum(mask)

    data_tuple_subset = CitationMicrophysData(n_times = n_times,
                                              times_utc = data_tuple.times_utc[mask],
                                              latitude = data_tuple.latitude[mask],
                                              longitude = data_tuple.longitude[mask],
                                              altitude = data_tuple.altitude[mask],
                                              T = data_tuple.T[mask],
                                              P = data_tuple.P[mask],
                                              lwc_King = data_tuple.lwc_King[mask],
                                              twc_Nevz = data_tuple.twc_Nevz[mask],
                                              lwc_Nevz = data_tuple.lwc_Nevz[mask],
                                              lwc_CDP = data_tuple.lwc_CDP[mask])
    return data_tuple_subset


def test():

    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/UND_cloud_micro/2012_01_19_14_48_04.gcpex',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/UND_cloud_micro/2012_01_27_01_55_33.gcpex',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/UND_cloud_micro/2012_01_28_15_49_01.gcpex']

    data = load(fpaths)

    subset_data = subset_loc(data, 44.18, 44.28, -79.83, -79.73)

    #for i_time in range(data.n_times):
    #    print(data.times_utc[i_time], data.altitude[i_time])

    for i_time in range(subset_data.n_times):
        print(subset_data.times_utc[i_time], subset_data.altitude[i_time])


if __name__ == '__main__':
    test()

