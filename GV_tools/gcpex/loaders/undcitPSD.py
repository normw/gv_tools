#!/usr/bin/env python3


"""Structure and methods for UND Citation combined particle spectrum data
"""

#--------------------------------------------------------------------------
# Structure and methods for UND Citation combined particle spectrum data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


#For use with the combined spectrum files from GCPEx Citation observations
#e.g, 20120128_154905.comb.spectrum.1Hz.nc, that combine 2DC/CIP and HVPS data.

import numpy
import collections
import netCDF4

_MISSING_FLOAT = -999.
_MISSING_INT = -999


CitationPSDData = collections.namedtuple('CitationPSDData', ['n_times',
                                                             'times_utc',
                                                             'Di_mid',
                                                             'Di',
                                                             'N_Di'])


def _bin_bounds(Di_mid):

    N_bins = Di_mid.shape[0]
    N_bounds = N_bins + 1
    Di = numpy.zeros((N_bounds,), dtype=type(Di_mid))
    delta_D = Di_mid[1] - Di_mid[0]
    Di[0] = Di_mid[0] - delta_D/2.
    for i_D in range(N_bins):
        delta_D_left = Di_mid[i_D] - Di[i_D]
        Di[i_D+1] = Di_mid[i_D] + delta_D_left
    return Di


def _load_single(fpath):

    data_tuple = None

    f_ptr = netCDF4.Dataset(fpath, 'r')

    #Get the date of data collection and create a corresponding datetime64 object
    #to use as a base time

    elements = f_ptr.startdate.split('-')
    year = int(elements[0])
    month = int(elements[1])
    day = int(elements[2])

    base_time = numpy.datetime64('%4d-%02d-%02dT00:00:00' %(year, month, day))

    #Process each variable
    tmp_var = f_ptr['Concentration']
    N_Di = numpy.ma.masked_values(tmp_var[:], tmp_var._FillValue)
    numpy.ma.set_fill_value(N_Di, _MISSING_FLOAT)
    N_Di /= 1000.                 #Convert from m^_4 to m^-3 mm^-1


    #Bin_Size contains the mid points of the size bins
    tmp_var = f_ptr['Bin_Size']
    Di_mid = tmp_var[:]/1000.               #Convert from micrometers to mm
    
    #Get the bin bounds from the bin midpoints
    Di = _bin_bounds(Di_mid)

    time_offset_var = f_ptr['time']
    time_delta = numpy.array([numpy.timedelta64(x.astype('int64'), 's') for x in time_offset_var[:]])
    
    times_utc = base_time + time_delta

    n_times = times_utc.shape[0]

    data_tuple = CitationPSDData(n_times = n_times,
                                 times_utc = times_utc,
                                 Di = Di,
                                 Di_mid = Di_mid,
                                 N_Di = N_Di)

    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))
    print(len(data_tuple_list))

    #Check that bin bounds don't change 
    Di = data_tuple_list[0].Di
    Di_mid = data_tuple_list[0].Di_mid
    for data_tuple in data_tuple_list[1:]:
        assert numpy.all(numpy.equal(data_tuple.Di, Di))
        assert numpy.all(numpy.equal(data_tuple.Di_mid, Di_mid))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure time is strictly increasing
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        n_times = times_utc.shape[0]
        N_Di = numpy.ma.concatenate([getattr(x, 'N_Di') for x in data_tuple_list], axis = 0)        
        data_tuple = CitationPSDData(n_times = n_times,
                                     times_utc = times_utc,
                                     Di = Di,
                                     Di_mid = Di_mid,
                                     N_Di = N_Di)
    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.fd12p._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0
    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)
    return data_tuple



def test():
    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/NCAR_probes/20120128_154905.comb.spectrum.1Hz.nc',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/NCAR_probes/20120130_224020.comb.spectrum.1Hz.nc',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Aircraft/Citation/NCAR_probes/20120212_030100.comb.spectrum.1Hz.nc']
    data = load(fpaths)

    for i_time in range(data.n_times):
        print(data.times_utc[i_time])


if __name__ == '__main__':
    test()

