#!/usr/bin/env python3


"""Structure and methods for radiosonde data
"""

#--------------------------------------------------------------------------
# Structure and methods for radiosonde data 
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


"""Load Sonde data structure

Copyright (c) 2017 Norman Wood.  All rights reserved.

This file is part of GV_tools.  It is subject to the license terms of
the BSD-3-Clause license, the full text of which is available in the
LICENSE file found in the top-level directory of the GV_tools
distribution.  No part of GV_tools, including this file, may be copied,
modified, propagated, or distributed except according to the terms
contained in the LICENSE file.
"""


import os
import numpy
import collections

from ...utilities import tools
from . import diagnostics

SondeData = collections.namedtuple('SondeData', ['n_times',
                                                 'times_utc',
                                                 'lat',
                                                 'lon',
                                                 'alt',
                                                 'pres',
                                                 'temp',
                                                 'temp_d',
                                                 'r_mix',
                                                 'wspd',
                                                 'wdir'])



#File properties
#Most files have 19 data elements per line, one has 20: it inserts
#a column for elevation angle, just after the azimumth angle column
#(index 12) and before the range column.  This affects the indices
#for the lat and lon columns (see var_specs? below).
_MISSING_VALUE_FLAG = -32768.



#Conversion routines to create masked arrays with desired units
#This is specific to these sonde files, don't put in ..utilities.tools.
def _convert_time(data_list, missing_value_flag, base_time):
    time_offset_seconds = numpy.array(data_list)
    omit_mask_finite = numpy.logical_not(numpy.isfinite(time_offset_seconds))
    if missing_value_flag is not None:
        omit_mask_missing = numpy.equal(time_offset_seconds, missing_value_flag)
        omit_mask = numpy.logical_or(omit_mask_finite, omit_mask_missing)
    else:
        omit_mask = omit_mask_missing
    times_utc = numpy.array([base_time + numpy.timedelta64(int(x), 's') for idx, x in enumerate(time_offset_seconds) if not omit_mask[idx]])
    return times_utc, omit_mask
    

#Var specs
#Defines the members in the namedtuple, the units, the numeric type,
#the column in the data file from which the data are sourced, and the
#conversion function used to produce the namedtuple member.
#Two versions because of two different file formats for GCPEx

#member name, numerical type, units, conversion function, datafile column
var_specs1 = [ ('times_utc', '-', '-', _convert_time, 0, {'missing_value_flag': None, 'base_time': None}),
               ('lat', '<f4', 'degrees', tools.list_to_MA_float, 15, {'missing_value_flag': None, 'omit_mask': None}),
               ('lon', '<f4', 'degrees', tools.list_to_MA_float, 14, {'missing_value_flag': None, 'omit_mask': None}),
               ('alt', '<f4', 'm', tools.list_to_MA_float, 6, {'missing_value_flag': None, 'omit_mask': None}),
               ('pres', '<f4', 'hPa', tools.list_to_MA_float, 7, {'missing_value_flag': None, 'omit_mask': None}),
               ('temp', '<f4', 'K', tools.list_to_MA_float, 2, {'missing_value_flag': None, 'omit_mask': None}),
               ('temp_d', '<f4', 'K', tools.list_to_MA_float, 8, {'missing_value_flag': None, 'omit_mask': None}),
               ('r_mix', '<f4', 'g/kg', tools.list_to_MA_float, 9, {'missing_value_flag': None, 'omit_mask': None}),
               ('wspd', '<f4', 'm/s', tools.list_to_MA_float, 11, {'missing_value_flag': None, 'omit_mask': None}),
               ('wdir', '<f4', 'degrees', tools.list_to_MA_float, 10, {'missing_value_flag': None, 'omit_mask': None})]

var_specs2 = [ ('times_utc', '-', '-', _convert_time, 0, {'missing_value_flag': None, 'base_time': None}),
               ('lat', '<f4', 'degrees', tools.list_to_MA_float, 16, {'missing_value_flag': None, 'omit_mask': None}),
               ('lon', '<f4', 'degrees', tools.list_to_MA_float, 15, {'missing_value_flag': None, 'omit_mask': None}),
               ('alt', '<f4', 'm', tools.list_to_MA_float, 6, {'missing_value_flag': None, 'omit_mask': None}),
               ('pres', '<f4', 'hPa', tools.list_to_MA_float, 7, {'missing_value_flag': None, 'omit_mask': None}),
               ('temp', '<f4', 'K', tools.list_to_MA_float, 2, {'missing_value_flag': None, 'omit_mask': None}),
               ('temp_d', '<f4', 'K', tools.list_to_MA_float, 8, {'missing_value_flag': None, 'omit_mask': None}),
               ('r_mix', '<f4', 'g/kg', tools.list_to_MA_float, 9, {'missing_value_flag': None, 'omit_mask': None}),
               ('wspd', '<f4', 'm/s', tools.list_to_MA_float, 11, {'missing_value_flag': None, 'omit_mask': None}),
               ('wdir', '<f4', 'degrees', tools.list_to_MA_float, 10, {'missing_value_flag': None, 'omit_mask': None})]


def _extract_data(line, var_specs, N_elements_expected):
    #Select data from the line, ordered following the order of the source datafile column numbers in var_specs
    data = None
    elements = line.split()
    N_elements = len(elements)
    if N_elements != N_elements_expected:
        sys.stderr.write('Wrong number of data elements extracted in loaders.sonde._extract_data()\n')
        assert 0
    for var_spec in var_specs:
        idx = var_spec[4]
        try:
            datum = int(elements[idx])
        except ValueError:
            datum = float(elements[idx])
        try:
            data.append(datum)
        except AttributeError:
            data = [datum,]
    return data


def _load_single(fpath):

    data_tuple = None

    with open(fpath, 'r') as f_in:
        lines = f_in.readlines()

    #Get the date and time of the sonde start from the filename
    #and create a corresponding datetime64 object to use
    #as a base time
    (path, fname) = os.path.split(fpath)
    (obs_type, timestring) = fname.split('-')
    elements = timestring.split('_')
    year = int(elements[0][0:4])
    month = int(elements[0][4:6])
    day = int(elements[0][6:8])
    hour = int(elements[1][0:2])
    minute = int(elements[1][2:4])
    base_time = numpy.datetime64('%4d-%02d-%02dT%02d:%02d' %(year, month, day, hour, minute))

    #Check to see which var_specs to use and where the data starts
    N_elements = len(lines[-1].split())
    if N_elements == 19:
        var_specs = var_specs1
        linenum_data_start = 39
    elif N_elements == 20:
        var_specs = var_specs2
        linenum_data_start = 40
    else:
        sys.stderr.write('Unrecognized number of data elements per line in loaders.sonde._load_single() for fpath %s\n' %(fpath,))
        assert 0
    
    #Set the missing_flag_values for the selected columns
    for i_var, var_spec in enumerate(var_specs):
        var_spec[5]['missing_value_flag'] = _MISSING_VALUE_FLAG

    #Construct a temporary data buffer
    N_vars = len(var_specs)
    data_buffer = [[] for i_var in range(N_vars)]
    
    #Fill the buffer
    for line in lines[linenum_data_start:]:
        data = _extract_data(line, var_specs, N_elements)
        if data is not None:
            for i_var in range(N_vars):
                data_buffer[i_var].append(data[i_var])

    n_times = len(data_buffer[0])

    #Build the list of namedtuple fields
    data_fields = []
    data_fields.append(n_times)

    #Do the times_utc variable first, to get the omit_mask up front
    omit_mask = None
    for i_var, var_spec in enumerate(var_specs):
        varname = var_spec[0]
        if varname == 'times_utc':
            fn = var_spec[3]
            kwargs = var_spec[5]
            kwargs['base_time'] = base_time
            data_field, omit_mask = fn(data_buffer[i_var], **kwargs)
            data_fields.append(data_field)
    for i_var, var_spec in enumerate(var_specs):
        varname = var_spec[0]
        if varname != 'times_utc':
            fn = var_spec[3]
            kwargs = var_spec[5]
            data_field = fn(data_buffer[i_var], **kwargs)
            data_fields.append(data_field)

    del data_buffer

    #Create the MicrophysData namedtuple
    data_tuple = SondeData(*data_fields)

    return data_tuple

def _concatenate(fpaths):

    data_tuple = None
    
    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure that time is strictly increasing
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        lat = numpy.ma.concatenate([getattr(x, 'lat') for x in data_tuple_list])
        lon = numpy.ma.concatenate([getattr(x, 'lon') for x in data_tuple_list])
        alt = numpy.ma.concatenate([getattr(x, 'alt') for x in data_tuple_list])
        pres = numpy.ma.concatenate([getattr(x, 'pres') for x in data_tuple_list])
        temp = numpy.ma.concatenate([getattr(x, 'temp') for x in data_tuple_list])
        temp_d = numpy.ma.concatenate([getattr(x, 'temp_d') for x in data_tuple_list])
        r_mix = numpy.ma.concatenate([getattr(x, 'r_mix') for x in data_tuple_list])
        wspd = numpy.ma.concatenate([getattr(x, 'wspd') for x in data_tuple_list])
        wdir = numpy.ma.concatenate([getattr(x, 'wdir') for x in data_tuple_list])
        n_times = times_utc.shape[0]
        

        data_tuple = SondeData(n_times = n_times,
                               times_utc = times_utc,
                               lat = lat,
                               lon = lon,
                               alt = alt,
                               pres = pres,
                               temp = temp,
                               temp_d = temp_d,
                               r_mix = r_mix,
                               wspd = wspd,
                               wdir = wdir)

    else:
        sys.stderr.write('times_utc values not strictly increasiung in loaders.sonde._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0

    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple


def test():
    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Sonde/gcpex_radiosonde_CARE-20120128_1500z_edt_010.tsv',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Sonde/gcpex_radiosonde_CARE-20120128_1937z_edt_011.tsv']
    data = load(fpaths)

    frac_days = tools.datetime64_to_frac_day(data.times_utc)
    frac_hrs = frac_days*24.
    for i_time, frac_hr in enumerate(frac_hrs):
        print(7*'%12.6f ' %(frac_hr, data.alt[i_time], data.pres[i_time], data.temp[i_time], data.temp_d[i_time], data.wspd[i_time], data.wdir[i_time]))

if __name__ == "__main__":
    test()
