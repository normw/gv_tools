#!/usr/bin/env python3


"""Structure and methods for FD12P data
"""

#--------------------------------------------------------------------------
# Structure and methods for FD12P data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

import sys
import os
import numpy
import collections
from . import diagnostics

FD12PData = collections.namedtuple('FD12PData', ['n_times',
                                                 'measurement_interval_h',
                                                 'times_utc', 
                                                 'precip_rate'])



_MISSING_PRECIP_RATE = -999.
_FD12P_DELTA_T_HR = 1./60.

def datestring(fpath):
    directory, fname = os.path.split(fpath)
    fname_parts = fname.split('_')
    return fname_parts[2]

def _load_single(fpath):
    #Get the date info from the file name
    directory, filename = os.path.split(fpath)
    this_datestring = datestring(fpath)
    year = int(this_datestring[0:4])
    month = int(this_datestring[4:6])
    day = int(this_datestring[6:8])

    datetime_list = []
    precip_rate_list = []
    
    with open(fpath, 'r') as f_in:
        lines = f_in.readlines()

    for line in lines:
        time, data = line.split(',')
        time_parts = time.split(':')

        try:
            hours = int(time_parts[0])
            minutes = int (time_parts[1])
            seconds = int(time_parts[2])
            datetime_str = '%4d-%02d-%02dT%02d:%02d:%09.6f' %(year, month, day, hours, minutes, seconds)
            datetime_value = numpy.datetime64(datetime_str)
        except (ValueError, IndexError):
            #No timestamp for this data, skip to next line
            continue

        #Otherwise, try to get the precip rate
        measurements = data.split()
        try:
            precip_rate = float(measurements[8])
            if precip_rate < 0.:
                precip_rate = _MISSSING_PRECIP_RATE
        except ValueError:
            precip_rate = _MISSING_PRECIP_RATE


        datetime_list.append(datetime_value)
        precip_rate_list.append(precip_rate)

    datetime_array = numpy.array(datetime_list, dtype='datetime64')
    precip_rate_array = numpy.ma.masked_values(precip_rate_list, _MISSING_PRECIP_RATE)

    n_times = datetime_array.shape[0]
    data_tuple = FD12PData(n_times = n_times,
                           measurement_interval_h = _FD12P_DELTA_T_HR,
                           times_utc = datetime_array,
                           precip_rate = precip_rate_array)
    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure time is strictly increasing and all measurement_interval_h values are the same
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        tmp = numpy.array([getattr(x, 'measurement_interval_h') for x in data_tuple_list])
        if numpy.allclose(tmp, tmp[0]): 
            precip_rate = numpy.ma.concatenate([getattr(x, 'precip_rate') for x in data_tuple_list])
            n_times = times_utc.shape[0]
            measurement_interval_h = tmp[0]
            data_tuple = FD12PData(n_times = n_times,
                                   measurement_interval_h = measurement_interval_h,
                                   times_utc = times_utc,
                                   precip_rate = precip_rate)
        else:
            sys.stderr.write('Inconsistent values for measurement_interval_h in loaders.fd12p._concatenate()\n')
            assert 0
    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.fd12p._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0

    return data_tuple


def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple

def test():
    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/FD12P/Huronia/gcpex_vis_20120128_EC_HURONIA.txt',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/FD12P/Huronia/gcpex_vis_20120129_EC_HURONIA.txt',
              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/FD12P/Huronia/gcpex_vis_20120130_EC_HURONIA.txt']
    data = load(fpaths)

    for i_time in range(data.n_times):
        print(data.times_utc[i_time], data.precip_rate[i_time])


if __name__ == '__main__':
    test()
