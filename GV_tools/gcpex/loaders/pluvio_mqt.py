#!/usr/bin/env python3


"""Structure and methods for Pluvio data as rescaled for Marquette obs
"""

#--------------------------------------------------------------------------
# Structure and methods for Pluvio data as rescaled for Marquette obs
#
# Copyright (c) 2019 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


"""Load Pluvio data structure

Copyright (c) 2019 Norman Wood.  All rights reserved.

This file is part of GV_tools.  It is subject to the license terms of
the BSD-3-Clause license, the full text of which is available in the
LICENSE file found in the top-level directory of the GV_tools
distribution.  No part of GV_tools, including this file, may be copied,
modified, propagated, or distributed except according to the terms
contained in the LICENSE file.

These routines are for use with the Pluvio data rescaled to match the snow
stake field accumulations at the Marquette site.
"""

import sys
import numpy
import collections
import h5py
from . import diagnostics

PluvioData = collections.namedtuple('PluvioData', ['n_times', 
                                                   'times_utc', 
                                                   'accumulation'])
def _load_single(fpath):

    data_tuple = None

    datetime_list = []
    accumulation_list = []
    
    f_ptr = h5py.File(fpaht, 'r')

    datetime_array = numpy.array([numpy.datetime64(x) for x in f_ptr['time_utc_iso'][:]])
    accumulation_array = f_ptr['accum'][:]

    n_times = datetime_array.shape[0]
    accumulation_array = numpy.array(accumulation_list, dtype=numpy.float)

    datetime_array = numpy.array(datetime_list, dtype=numpy.datetime64)

    data_tuple = PluvioData(n_times = n_times,
                            times_utc = datetime_array,
                            accumulation = accumulation_array)

    return data_tuple


def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        accumulation = numpy.concatenate([getattr(x, 'accumulation') for x in data_tuple_list])
        n_times = times_utc.shape[0]

        data_tuple = PluvioData(n_times = n_times,
                                times_utc = times_utc,
                                accumulation = accumulation)
    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.pluvio._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0
        
    return data_tuple


def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple


def test():
    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Pluvio_alt/pluvio200_all.care',]
    data = load(fpaths)

    for i_time in range(data.n_times):
        print(data.times_utc[i_time], data.accumulation[i_time])


if __name__ == '__main__':
    test()
