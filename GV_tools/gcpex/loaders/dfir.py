#!/usr/bin/env python3


"""Structure and methods for Double-Fence International Reference data
"""

#--------------------------------------------------------------------------
# Structure and methods for Double-Fence International Reference data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import sys
import xlrd
import numpy
import datetime
import collections
from . import diagnostics

DFIRData = collections.namedtuple('DFIRData', ['n_times',
                                               'times_utc',
                                               'accumulation'])

_ACCUMULATION_MISSING = -999.

def _load_single(fpath):

    data_tuple = None

    datetime_list = []
    accumulation_list = []
    
    wb = xlrd.open_workbook(fpath)
    datasheet = wb.sheet_by_name('Sheet1')

    N_rows = datasheet.nrows

    orphaned_accumulations = []
    for i_obs in range(1,N_rows):
        observation = datasheet.row(i_obs)
        datetime_string = observation[1].value
        accumulation_string = observation[5].value
        try:
            accumulation_mm = float(accumulation_string)
        except ValueError:
            accumulation_mm = None
        try:
            datetime_value = numpy.datetime64(datetime.datetime.strptime(datetime_string, '%y:%m:%d:%H:%M'), 's')
        except ValueError:
            datetime_value = None

        if datetime_value is not None:
            #Valid timestamp
            datetime_list.append(datetime_value)
            if accumulation_mm is not None:
                #Valid accumulation for this timestamp
                #Add the current accumulation to any prior orphaned
                #accumulations and empty the list of orphans
                while orphaned_accumulations:
                    accumulation_mm += orphaned_accumulations.pop()
                accumulation_list.append(accumulation_mm)
            else:
                #No valid accumulation for this timestamp
                if orphaned_accumulations:
                    #If there are orphaned accumulations, treat the current
                    #accumulation as zero, then add any prior orphaned
                    #accumulations and empty the list of orphans
                    accumulation_mm = 0.
                    while orphaned_accumulations:
                        accumulation_mm += orphaned_accumulations.pop()
                    accumulation_list.append(accumulation_mm)
                else:
                    #If no valide accumulation for this timestamp and
                    #no orphaned accumulations, set the accumulation as missing
                    accumulation_list.append(_ACCUMULATION_MISSING)
        else:
            #No timestamp for this data
            #Do nothing except, if there is a valid accumulation
            #add it to the orphan list
            if accumulation_mm is not None:
                orphaned_accumulations.append(accumulation_mm)
                
        #accumulation_list.append(accumulation_mm)
        #datetime_list.append(datetime_value)

    datetime_array = numpy.array(datetime_list, dtype='datetime64')
    accumulations_array = numpy.ma.masked_values(accumulation_list, _ACCUMULATION_MISSING)

    n_times = datetime_array.shape[0]
    
    data_tuple = DFIRData(n_times = n_times,
                          times_utc = datetime_array,
                          accumulation = accumulations_array)

    return data_tuple

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
    #Make sure time is strictly increasing 
    if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
        accumulation = numpy.ma.concatenate([getattr(x, 'accumulation') for x in data_tuple_list])
        n_times = times_utc.shape[0]
        data_tuple = DFIRData(n_times = n_times,
                              times_utc = times_utc,
                              accumulation = accumulation)
    else:
        sys.stderr.write('times_utc values not strictly increasing in loaders.dfir._concatenate()\n')
        diagnostics.sequential_time_error_handler(data_tuple_list)
        assert 0

    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple

def test():
    fpaths = '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Manual_precip/gcpex_manual_observation_precipitation_EC_CARE_v2_corrected.xls'

#    fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Manual_precip/gcpex_manual_observation_precipitation_EC_CARE_v2_corrected.xls',
#              '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/Manual_precip/gcpex_manual_observation_precipitation_EC_CARE_v2_corrected.xls']
             
    data = load(fpaths)
    
    for i_time in range(data.n_times):
        print(data.times_utc[i_time], data.accumulation[i_time])

if __name__ == "__main__":
    test()
