#!/usr/bin/env python3


"""Miscellaneous utilities
"""

#--------------------------------------------------------------------------
# Miscellaneous utilities
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

import sys
import numpy

#Needed for some of the time conversion functions
import datetime


def nc4_to_ma(nc4_variable):
    #nc4_variable is an instance of netCDF4.Variable
    #if nc4_variable has attributes _FillValue or missing_value, and it has
    #elements whose values match those attributes, the data values nc4_variable[:]
    #will be an instance of MaskedArray, so just return it.  Otherwise it is a
    #plain ndarray, so convert it to a MaskedArray and return it.
    result = None
    if isinstance(nc4_variable[:], numpy.ma.MaskedArray):
        result = nc4_variable[:]
    else:
        result = numpy.ma.array(nc4_variable[:])
    return result
    #try:
    #    result = numpy.ma.masked_values(netcdf4_variable[:], float(netcdf4_variable.missing_value))
    #except (AttributeError, ValueError) as err:
    #    result = numpy.ma.masked_array(netcdf4_variable[:])
    #return result

def nc4_to_ma_datetime(netcdf4_variable):
    result = None
    try:
        tmp_array = numpy.ma.masked_values(netcdf4_variable[:], float(netcdf4_variable.missing_value))
        result = numpy.ma.asarray(tmp_array, dtype='datetime64[s]')
    except (AttributeError, ValueError) as err:
        result = numpy.ma.masked_array(netcdf4_variable[:], dtype='datetime64[s]')
    return result

def nc4_unixtime_to_datetime64(nc4_variable):
    #nc4_variable is an instance of netCDF4.Variable
    #Convert a netCDF4 variable containing Unix time to a NumPy datetime64 array
    #
    #Note that since time is a dimensional axis in most datasets, we don't want
    #the resulting numpy array to be a masked array.  Note that the data in the netcdf4_variable
    #will be stored in a masked array (by Python's netCDF4 package) if the data has any
    #values that match the netcd4_variable's _FillValue or missing_value attributes, if it
    #has them.

    data = None
    unixtime_data = nc4_variable[:]
    if not hasattr(unixtime_data, 'mask'):
        data = numpy.array(unixtime_data, dtype='datetime64[s]')
    else:
        bad_mask = numpy.ma.getmaskarray(unixtime_data)
        N_bad = numpy.sum(bad_mask)
        if N_bad > 0:    
            raise TimeDimensionError("Error:  Time data contains masked values")
        else:
            data = numpy.array(unixtime_data.filled(), dtype='datetime64[s]')
    return data

def list_to_MA_float(data_list, missing_value_flag=None, omit_mask=None):
    #Mask data that match missing_value_flag, omit data where omit_mask is True
    if missing_value_flag is not None:
        data = numpy.ma.masked_values([float(val) for val in data_list], missing_value_flag)
    else:
        data = numpy.ma.array([float(val) for val in data_list])
    if omit_mask is not None:
        data = data[numpy.logical_not(omit_mask)]
    return data

def list_to_MA_integer(data_list, missing_value_flag=None, omit_mask=None):
    #Mask data that match missing_value_flag, omit data where omit_mask is True
    if missing_value_flag is not None:
        data = numpy.ma.masked_equal([int(val) for val in data_list], missing_value_flag)
    else:
        data = numpy.ma.array([int(val) for val in data_list])
    if omit_mask is not None:
        data = data[numpy.logical_not(omit_mask)]
    return data

def hms_to_datetime64(hms_list, base_time=None, hms_missing=None):
    if base_time is None:
        print('Error in tools.hms_to_datetime64:  base_time kwarg required')
        assert 0

    if hms_missing is not None:
        data = numpy.array(
                   [base_time + numpy.timedelta64(hms_to_seconds(x), 's')
                    for x in hms_list if x not in hms_missing])
    else:
        data = numpy.array(
                   [base_time + numpy.timedelta64(hms_to_seconds(x), 's')
                    for x in hms_list])

    return data

    
def datetime_to_frac_day(datetime_array):
    frac_days = numpy.empty_like(datetime_array, dtype=float)
    for i_time, datetime_value in enumerate(datetime_array):
        frac_days[i_time] = ((datetime_value.second/60. + datetime_value.minute)/60. + datetime_value.hour)/24.
    return frac_days

def datetime_to_frac_doy(datetime_array):
    frac_doys = numpy.empty_like(datetime_array, dtype=float)
    for i_time, datetime_value in enumerate(datetime_array):
        frac_doys[i_time] = datetime_value.timetuple().tm_yday + (datetime_value.hour + datetime_value.minute/60. + datetime_value.second/3600.)/24.
    return frac_doys

def datetime64_to_frac_doy(datetime64_data):
    datetime64_data = numpy.asarray(datetime64_data)
    scalar_input = False
    if datetime64_data.ndim == 0:
        datetime64_data = datetime64_data[None]
        scalar_input = True
    frac_doys = numpy.empty_like(datetime64_data, dtype=float)
    for i_time, datetime64_value in enumerate(datetime64_data):
        x = datetime64_value.astype(datetime.datetime)
        frac_doys[i_time] = x.timetuple().tm_yday + (x.hour + x.minute/60. + x.second/3600.)/24.
    if scalar_input:
        return numpy.squeeze(frac_doys)
    return frac_doys
   
def datetime64_to_frac_day(datetime64_data):
    datetime64_data = numpy.asarray(datetime64_data)
    scalar_input = False
    if datetime64_data.ndim == 0:
        datetime64_data = datetime64_data[None]
        scalar_input = True
    frac_days = numpy.empty_like(datetime64_data, dtype=float)
    for i_time, datetime64_value in enumerate(datetime64_data):
        x = datetime64_value.astype(datetime.datetime)
        frac_days[i_time] = ((x.second/60. + x.minute)/60. + x.hour)/24.
    if scalar_input:
        return numpy.squeeze(frac_days)
    return frac_days

def datetime64_to_string(datetime64_value):
    x = datetime64_value.astype(datetime.datetime)
    frac_seconds = x.microsecond/1.e6 + x.second
    timestring = '%4d-%02d-%02dT%02d:%02d:%06.3f' %(
                 x.year,
                 x.month,
                 x.day,
                 x.hour,
                 x.minute,
                 frac_seconds)
    return timestring
    
                    

def seconds_to_HMS(total_seconds):
    #This is a better way than doing divisions, taking integers, then
    #calculating remainders via subtraction.  That method results in small imprecisions
    #when the seconds are near 0. or 60. that lead to errors in the calculated hours,
    #minutes and seconds.  This approach seems more resistant to that.  See
    #https://stackoverflow.com/questions/775049/python-time-seconds-to-hms

    minutes, seconds = divmod(total_seconds, 60)
    hours, minutes = divmod(minutes, 60)

    #Handle seconds vanishingly close to 60.
    if numpy.isclose(seconds, 60.):
        seconds = 0.
        minutes += 1
    if minutes >= 60:
        hours += 1
        minutes = minutes - 60

    return hours, minutes, seconds

def frac_day_to_HMS(frac_day):
    total_seconds = frac_day*86400.
    hours, minutes, seconds = seconds_to_HMS(total_seconds)
    return (hours, minutes, seconds)

def day_of_year_to_month_day(day_of_year, year):
    date_string = '%4d %03d' %(year, day_of_year)
    date = datetime.datetime.strptime(date_string, '%Y %j')
    year = date.year
    month = date.month
    day = date.day
    return (year, month, day)

def hms_to_seconds(hms_string):
    (hours, minutes, seconds) = (int(x) for x in hms_string.split(':'))
    total_seconds = hours*3600 + minutes*60 + seconds
    return total_seconds


class TimeDimensionError(Exception):
   pass
