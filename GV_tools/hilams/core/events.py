#!/usr/bin/env python3

"""Description
"""

#--------------------------------------------------------------------------
# HiLaMS event definitions
#
# Copyright (c) 2015 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


"""HiLaMS event definitions

Copyright (c) 2025 Norman Wood.  All rights reserved.

This file is part of GV_tools.  It is subject to the license terms of
the BSD-3-Clause license, the full text of which is available in the
LICENSE file found in the top-level directory of the GV_tools
distribution.  No part of GV_tools, including this file, may be copied,
modified, propagated, or distributed except according to the terms
contained in the LICENSE file.
"""


import numpy

event_dict = {'E01':('2016-12-06T00:00:00', '2017-04-27T00:00:00'),}


def test():
    for event in event_dict.keys():
        t_start = numpy.datetime64(event_dict[event][0])
        t_end = numpy.datetime64(event_dict[event][1])
        print(t_start, t_end)

if __name__ == "__main__":
    test()
