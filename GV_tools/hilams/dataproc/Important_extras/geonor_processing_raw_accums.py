#!/usr/bin/env python

import os
import glob
import math
import netCDF4
import numpy

data_dir = '/boltzmann/data6/FieldExp/HiLaMS/Haukeliseter/Meteorology/NetCDF'
glob_pattern = 'Met_*_Haukeliseter.nc'

def date(fpath):
    directory, fname = os.path.split(fpath)
    datestring = fname.split('_')[1]
    this_date = numpy.datetime64('%4s-%02s-%02s' %(datestring[0:4], datestring[4:6], datestring[6:8]), dtype='datetime64')
    return this_date

    
def main():    

    fpaths = glob.glob(os.path.join(data_dir, glob_pattern))

    #Sort the files by datestring so they are in order of increasing date
    fpaths.sort(key=date)
    #Starting time of the dataset
    t_init = date(fpaths[0])

    precip_flag_array_list = []
    times_array_list = []
    residuals_list = []
    precip_array_list = []
    air_temp_array_list = []
    gauge1_array_list = []
    gauge2_array_list = []
    gauge3_array_list = []
    
    precip1_times_list = []
    precip1_accum_list = []
    precip2_times_list = []
    precip2_accum_list = []
    precip3_times_list = []
    precip3_accum_list = []

    for fpath in fpaths:
        #Get time information
        (directory, fname) = os.path.split(fpath)
        fname_parts = fname.split('_')
        year = int(fname_parts[1][0:4])
        month = int(fname_parts[1][4:6])
        day = int(fname_parts[1][6:8])
        hour = 0
        minute = 0
        second = 0
        dset = netCDF4.Dataset(fpath, 'r')
        secs = dset.variables['sec']
        n_times = len(dset.dimensions['time'])
        t_start = numpy.datetime64('%4d-%02d-%02dT%02d:%02d:%02d' %(year, month, day, hour, minute, second), dtype='datetime64[ms]')
        times = t_start + numpy.array([numpy.timedelta64(x, 's') for x in secs])
        air_temp = dset.variables['air_temp']
        gauge1 = dset.variables['double_fence_accum']
        gauge2 = dset.variables['single_south_accum']
        gauge3 = dset.variables['single_upstream_accum']
    
        pwd_data = dset.variables['weather_sensor']
        pwd_precip_intensity = pwd_data[:, 1]
    
        precip_flag = numpy.ma.greater(pwd_precip_intensity, 0.)

        times_array_list.append(times)
        air_temp_array_list.append(air_temp[:])
        precip_flag_array_list.append(precip_flag)
        gauge1_array_list.append(gauge1[:])
        gauge2_array_list.append(gauge2[:])
        gauge3_array_list.append(gauge3[:])

    times_array = numpy.ma.concatenate(times_array_list)
    del times_array_list
    air_temp_array = numpy.ma.concatenate(air_temp_array_list)
    del air_temp_array_list
    precip_flag_array = numpy.ma.concatenate(precip_flag_array_list)
    del precip_flag_array_list
    gauge1_array = numpy.ma.concatenate(gauge1_array_list)
    del gauge1_array_list
    gauge2_array = numpy.ma.concatenate(gauge2_array_list)
    del gauge2_array_list
    gauge3_array = numpy.ma.concatenate(gauge3_array_list)
    del gauge3_array_list

    mask = numpy.argwhere(numpy.isnan(gauge1_array))
    gauge1_array[mask] = -999.
    mask = numpy.argwhere(numpy.isnan(gauge2_array))
    gauge2_array[mask] = -999.
    mask = numpy.argwhere(numpy.isnan(gauge3_array))
    gauge3_array[mask] = -999.

    with open('results_raw_accums1.txt', 'w') as f_out:
        for idx_t, time in enumerate(times_array):
            f_out.write('%s %15.5e %15.5e %15.5e\n' %(time, gauge1_array[idx_t, 0], gauge1_array[idx_t, 1], gauge1_array[idx_t, 2]))
    with open('results_raw_accums2.txt', 'w') as f_out:
        for idx_t, time in enumerate(times_array):
            f_out.write('%s %15.5e %15.5e %15.5e\n' %(time, gauge2_array[idx_t, 0], gauge2_array[idx_t, 1], gauge2_array[idx_t, 2]))
    with open('results_raw_accums3.txt', 'w') as f_out:
        for idx_t, time in enumerate(times_array):
            f_out.write('%s %15.5e %15.5e %15.5e\n' %(time, gauge3_array[idx_t, 0], gauge3_array[idx_t, 1], gauge3_array[idx_t, 2]))


if __name__ == '__main__':
    main()
