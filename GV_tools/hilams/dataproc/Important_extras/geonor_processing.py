#!/usr/bin/env python

import os
import glob
import math
import netCDF4
import numpy

data_dir = '/boltzmann/data6/FieldExp/HiLaMS/Haukeliseter/Meteorology/NetCDF'
glob_pattern = 'Met_*_Haukeliseter.nc'

def proc_wire_deltas(times, wire_data):
    n_measurements = times.shape[0]-1
    n_subsamples = n_measurements/5

    #Create a masked version of wire_data to work with
    wire_data_ma = numpy.ma.masked_invalid(wire_data)

    #Mask based on invalid data
    mask = numpy.isnan(wire_data_ma)
    wire_data_ma[mask] = numpy.ma.masked
    
    #Mask based on broken wires
    mask = numpy.ma.less(wire_data_ma, -15.)
    #Further evaluate:  Anywhere there is more than one wire masked, mask them all
    for idx_t in range(n_measurements):
        if numpy.sum(mask[idx_t, :]) > 1:
            mask[idx_t, :] = True
    wire_data_ma[mask] = numpy.ma.masked

    #Mask based on capacity
    mask = numpy.ma.greater(wire_data_ma, 1000.)
    wire_data_ma[mask] = numpy.ma.masked

    #Compute the deltas
    try:
        wire_deltas = numpy.ma.empty((n_subsamples,3), dtype=numpy.float)
    except ValueError:
        print n_subsamples, n_measurements
        assert 0
    times_of_deltas = numpy.ma.empty((n_subsamples,), dtype='datetime64[s]')

    for idx_t in range(n_subsamples):
        idx_prior = idx_t*5
        idx_current = min(idx_prior+5, n_measurements)
        times_of_deltas[idx_t] = times[idx_current]
        for idx_wire in range(3):
            wire_deltas[idx_t, idx_wire] = wire_data_ma[idx_current, idx_wire] - wire_data_ma[idx_prior, idx_wire]

    #Mask based on 'giant deltas'
    mask = numpy.ma.greater(wire_deltas,  25.)
    wire_deltas[mask] = numpy.ma.masked
    return times_of_deltas, wire_deltas

def subsample_precip_flag(times, precip_flag):
    n_measurements = times.shape[0]-1
    n_subsamples = n_measurements/5

    precip_flag_subsamples = numpy.ma.empty((n_subsamples,), dtype=bool)
    for idx_t in range(n_subsamples):
        idx_prior = idx_t*5
        idx_current = min(idx_prior+5, n_measurements)
        precip_flag_subsamples[idx_t] = numpy.any(precip_flag[idx_prior+1:idx_current])
    return precip_flag_subsamples

def subsample_air_temp(times, air_temp):
    n_measurements = times.shape[0]-1
    n_subsamples = n_measurements/5

    air_temp_subsample = numpy.ma.empty((n_subsamples,), dtype=bool)
    for idx_t in range(n_subsamples):
        idx_prior = idx_t*5
        idx_current = min(idx_prior+5, n_measurements)
        air_temp_subsample[idx_t] = numpy.ma_average(air_temp[idx_prior+1:idx_current])
    return air_temp_subsamples

def adjust_negatives(precip_array):
    negative_sum = numpy.sum(precip_array[precip_array < 0])
    if negative_sum < 0:
        positive_sum = numpy.sum(precip_array[precip_array > 0])
        if positive_sum < numpy.abs(negative_sum):
            #Problem - the negative values outweigh the positives
            precip_array[:] = 0.
            residual = positive_sum + negative_sum
            return precip_array, residual
        else:
            adjustment_total = 0.
            for idx_t in range(precip_array.shape[0]):
                if precip_array[idx_t] < 0.:
                    precip_array[idx_t] = 0.
                elif precip_array[idx_t] > 0.:
                    apportioned_adjustment = numpy.abs(negative_sum)*precip_array[idx_t]/positive_sum
                    adjustment = min(apportioned_adjustment, precip_array[idx_t])
                    #try:
                    #    adjustment = numpy.min(apportioned_adjustment, precip_array[idx_t])
                    #except ValueError:
                    #    print idx_t
                    #    print apportioned_adjustment
                    #    print precip_array[idx_t]
                    #    assert 0
                    precip_array[idx_t] = precip_array[idx_t] - adjustment
                    adjustment_total += adjustment
            residual = adjustment_total + negative_sum
            return precip_array, residual
    else:
        residual = 0
        return precip_array, residual

def proc_weights(wire_deltas):

    weights = None
    n_time_samples = wire_deltas.shape[0]
    if n_time_samples > 0:
        means = numpy.ma.average(wire_deltas, axis=1)
        diffs = wire_deltas - means[..., numpy.newaxis]
        delta_variances = numpy.ma.sum(numpy.ma.power(diffs, 2), axis=0)/n_time_samples
        if numpy.ma.all(numpy.ma.greater(delta_variances, 0.)):
            weights = 1./delta_variances/numpy.ma.sum(1./delta_variances)
    return weights

def proc_precip(times, wire_data, precip_flag, air_temp, t_report_start):
    #accum_data is the time series of accumulations from each of the three wire
    #sensors on the Geonor gauge.  This algorithm implements the USCRN OAP 2.0
    #wavgCalc processing method.  The accumulation data are at 1-minute intervals,
    #and processing is into 5-minute accumulations

    times_of_deltas, wire_deltas = proc_wire_deltas(times, wire_data)
    weights = proc_weights(wire_deltas)

    mean_wire_deltas = numpy.ma.average(wire_deltas, axis=1, weights=weights)

    #Set non-precipitating deltas to zero (may not want to do this, see how it works)         
    precip_flag_subsampled = subsample_precip_flag(times, precip_flag)
    mask = numpy.logical_not(precip_flag_subsampled)
    mean_wire_deltas[mask] = 0.

    idx_start = numpy.argmax(numpy.less(t_report_start, times_of_deltas))

    times_report = times_of_deltas[idx_start:]
    precip_report = mean_wire_deltas[idx_start:]
    air_temp_report = air_temp[idx_start:]

    precip_report, residual = adjust_negatives(precip_report)

    return times_report, precip_report, air_temp_report, residual


def date(fpath):
    directory, fname = os.path.split(fpath)
    datestring = fname.split('_')[1]
    this_date = numpy.datetime64('%4s-%02s-%02s' %(datestring[0:4], datestring[4:6], datestring[6:8]), dtype='datetime64')
    return this_date

    
def main():    

    fpaths = glob.glob(os.path.join(data_dir, glob_pattern))

    #Sort the files by datestring so they are in order of increasing date
    fpaths.sort(key=date)
    #Starting time of the dataset
    t_init = date(fpaths[0])

    precip_flag_array_list = []
    times_array_list = []
    residuals_list = []
    precip_array_list = []
    air_temp_array_list = []
    gauge1_array_list = []
    gauge2_array_list = []
    gauge3_array_list = []
    
    precip1_times_list = []
    precip1_accum_list = []
    precip1_air_temp_list = []
    precip2_times_list = []
    precip2_accum_list = []
    precip3_times_list = []
    precip3_accum_list = []

    for fpath in fpaths:
        #Get time information
        (directory, fname) = os.path.split(fpath)
        fname_parts = fname.split('_')
        year = int(fname_parts[1][0:4])
        month = int(fname_parts[1][4:6])
        day = int(fname_parts[1][6:8])
        hour = 0
        minute = 0
        second = 0
        dset = netCDF4.Dataset(fpath, 'r')
        secs = dset.variables['sec']
        n_times = len(dset.dimensions['time'])
        t_start = numpy.datetime64('%4d-%02d-%02dT%02d:%02d:%02d' %(year, month, day, hour, minute, second), dtype='datetime64[ms]')
        times = t_start + numpy.array([numpy.timedelta64(x, 's') for x in secs])
        air_temp = dset.variables['air_temp']
        gauge1 = dset.variables['double_fence_accum']
        gauge2 = dset.variables['single_south_accum']
        gauge3 = dset.variables['single_upstream_accum']
    
        pwd_data = dset.variables['weather_sensor']
        pwd_precip_intensity = pwd_data[:, 1]
    
        precip_flag = numpy.ma.greater(pwd_precip_intensity, 0.)

        times_array_list.append(times)
        air_temp_array_list.append(air_temp[:])
        precip_flag_array_list.append(precip_flag)
        gauge1_array_list.append(gauge1[:])
        gauge2_array_list.append(gauge2[:])
        gauge3_array_list.append(gauge3[:])

    times_array = numpy.ma.concatenate(times_array_list)
    del times_array_list
    air_temp_array = numpy.ma.concatenate(air_temp_array_list)
    del air_temp_array_list
    precip_flag_array = numpy.ma.concatenate(precip_flag_array_list)
    del precip_flag_array_list
    gauge1_array = numpy.ma.concatenate(gauge1_array_list)
    del gauge1_array_list
    gauge2_array = numpy.ma.concatenate(gauge2_array_list)
    del gauge2_array_list
    gauge3_array = numpy.ma.concatenate(gauge3_array_list)
    del gauge3_array_list


    #Set the masks for gauge servicing periods (determined by examining the raw gauge accumulation time series
    gauge1_array[15040:16490+1] = numpy.ma.masked
    gauge1_array[363585:363595+1] = numpy.ma.masked
    gauge1_array[366850:366895+1] = numpy.ma.masked

    gauge2_array[15020:16585+1] = numpy.ma.masked
    gauge2_array[173590:173600+1] = numpy.ma.masked
    gauge2_array[363580:363585+1] = numpy.ma.masked

    gauge3_array[16620:16700+1] =  numpy.ma.masked
    gauge3_array[173610:173630+1] = numpy.ma.masked
    gauge3_array[363570:363575+1] = numpy.ma.masked
        
    #Set up the times ranges for processing, 3 hours at a time as per the USCRN OAP
    delta_t = numpy.timedelta64(3600, 's')
    t_start = date(fpaths[0]) + 2*delta_t
    t_end = date(fpaths[-1])+24*delta_t
    precip_sum = 0.
    residual1_sum = 0.
    residual2_sum = 0.
    residual3_sum = 0.
    residual_list = []
    for t_current in numpy.arange(t_start, t_end, delta_t):
        print t_current
        t_block_start = t_current - 2*delta_t
        t_block_end = t_current + delta_t
        mask = numpy.ma.logical_and(numpy.ma.less_equal(t_block_start, times_array),
                                    numpy.ma.less_equal(times_array, t_block_end))
        n_times = numpy.sum(mask)
        if n_times > 0:
            this_times, this_precip, this_air_temp, this_residual = proc_precip(times_array[mask], gauge1_array[mask], precip_flag_array[mask], air_temp_array[mask], t_current)
            precip1_times_list.append(this_times)
            precip1_accum_list.append(this_precip)
            precip1_air_temp_list.append(this_air_temp)
            residual1_sum += this_residual
            this_times, this_precip, this_air_temp, this_residual = proc_precip(times_array[mask], gauge2_array[mask], precip_flag_array[mask], air_temp_array[mask], t_current)
            precip2_times_list.append(this_times)
            precip2_accum_list.append(this_precip)
            residual2_sum += this_residual
            this_times, this_precip, this_air_temp, this_residual = proc_precip(times_array[mask], gauge3_array[mask], precip_flag_array[mask], air_temp_array[mask], t_current)
            precip3_times_list.append(this_times)
            precip3_accum_list.append(this_precip)
            residual3_sum += this_residual

    precip1_times = numpy.concatenate(precip1_times_list)
    precip2_times = numpy.concatenate(precip2_times_list)
    precip3_times = numpy.concatenate(precip3_times_list)
    precip1_accum = numpy.concatenate(precip1_accum_list)
    precip2_accum = numpy.concatenate(precip2_accum_list)
    precip3_accum = numpy.concatenate(precip3_accum_list)
    precip1_air_temp = numpy.concatenate(precip1_air_temp_list)
    
    del precip1_times_list, precip2_times_list, precip3_times_list
    del precip1_accum_list, precip2_accum_list, precip3_accum_list

    assert numpy.array_equal(precip1_times, precip2_times)
    assert numpy.array_equal(precip1_times, precip3_times)
    assert numpy.array_equal(precip2_times, precip3_times)

    n_times = precip1_times.shape[0]
    with open('results.txt', 'w') as f_out:
        for idx_t in range(n_times):
            f_out.write('%s %15.5e %15.5e %15.5e %15.5e\n' %(precip1_times[idx_t], precip1_accum[idx_t], precip2_accum[idx_t], precip3_accum[idx_t], precip1_air_temp[idx_t]))

    print residual1_sum, numpy.ma.sum(precip1_accum)
    print residual2_sum, numpy.ma.sum(precip2_accum)
    print residual3_sum, numpy.ma.sum(precip3_accum)

if __name__ == '__main__':
    main()
