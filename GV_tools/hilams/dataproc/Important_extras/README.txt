This are some pieces of code that I've put together in the past that I think
could likely be useful for GV_tools.

In here right now are three Python codes for working with the Geonor
precipitation observations from Hauleliseter.   The originals are in
/csudisk/Projects/NSF_snow_vorticity/Analyses/Haukeliseter_precipitation_gauge_agreement/
See this directory for other possibly relevant codes 

geonor_processing_raw_accums.py:  Computes accumulations from each of the three
wire sensors on each Geonor gauge and dumps them out to text.  Useful for
visually examining the gauge sensor outputs.

geonor_processing_raw_diffs.py:  Computes incremental changes for each sensor
wire, applying some masking for indications of broken wires, sensors at their
upper limit, or too-large increments.  It then implement processing per the US
Climate Reference Network's Official Algorithm for Precipitation (search for
USCRN_OAP2_Description.pdf).  There is also a relevant paper, Leeper et al.,
2015, JTech.

geonor_processing.py:  This seems to be the more complete and final
implementation of the processing, and incorporates methods for changing temporal
sampling and looking at precipitation flag information from the present weather
sensor at Haukeliseter.






Leeper, R. D., M. A. Palecki, and E. Davis, 2015:  Methods to calculate
precipitation from weighing-bucket gauges with redundant depth measurements.  J.
Atmos. Oceanic Technol., 32, 1179-1190, doi:10.1175/JTECH-D-14-00185.1.





