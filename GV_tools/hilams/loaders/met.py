#!/usr/bin/env python3

"""Structure and methods for HiLaMS meteorology data
"""

import sys
import numpy
import collections
import netCDF4

MetData = collections.namedtuple('MetData', ['n_times',
                                             'times_utc',
                                             'T_ambient',
                                             'u_10m_mast1',
                                             'phi_10m_mast1',
                                             'u_10m_mast2',
                                             'phi_10m_mast2',
                                             'u_4m_mast2'])



def _load_single(fpath):

   dset = netCDF4.Dataset(fpath, 'r')
   ymd_var = dset.variables['YMD']
   seconds_var = dset.variables['sec']
   T_amb_var = dset.variables['air_temp']
   u_10m_mast1_var = dset.variables['10m_wind_speed_mast1']
   phi_10m_mast1_var = dset.variables['10m_wind_dir_mast1']
   u_10m_mast2_var = dset.variables['10m_wind_speed_mast2']
   phi_10m_mast2_var = dset.variables['10m_wind_dir_mast2']
   u_4m_mast2_var = dset.variables['4m_wind_speed_mast2']

   data_tuple = None
   N_times = ymd_var[:].shape[0]

   #Convert file times to datetime64 times_utc
   
   times_utc = numpy.zeros((N_times,), dtype='datetime64[s]')
   for i_time in range(N_times):
      ymd_str = '%s' %(ymd_var[i_time],)
      yr_str = ymd_str[0:4]
      mm_str = ymd_str[4:6]
      dd_str = ymd_str[6:8]

      base_time = numpy.datetime64('%s-%s-%sT00:00:00' %(yr_str, mm_str, dd_str))
      delta_time_s = numpy.timedelta64(int(seconds_var[i_time]), 's')
      times_utc[i_time] = base_time + delta_time_s

   data_tuple = MetData(n_times = N_times,
                        times_utc = times_utc,
                        T_ambient = T_amb_var[:],
                        u_10m_mast1 = u_10m_mast1_var[:],
                        phi_10m_mast1 = phi_10m_mast1_var[:],
                        u_10m_mast2 = u_10m_mast2_var[:],
                        phi_10m_mast2 = phi_10m_mast2_var[:],
                        u_4m_mast2 = u_4m_mast2_var[:])
   dset.close()
   return data_tuple


def _concatenate(fpaths):

   data_tuple = None

   data_tuple_list_tmp = []
   for fpath in fpaths:
      data_tuple_list_tmp.append(_load_single(fpath))

   times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list_tmp])

   #Make sure that time is strictly increasing
   if not numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
      #Bail out
      sys.stderr.write('times_utc values not strictly increasing in loaders.mrr._concatenate()\n')
      diagnostics.sequential_time_error_handler(data_tuple_list)
      assert 0
   else:
      data_tuple_list = data_tuple_list_tmp
   
   #Create the concatenated data tuple
   T_ambient = numpy.concatenate([getattr(x, 'T_ambient') for x in data_tuple_list])
   u_10m_mast1 = numpy.concatenate([getattr(x, 'u_10m_mast1') for x in data_tuple_list])
   phi_10m_mast1 = numpy.concatenate([getattr(x, 'phi_10m_mast1') for x in data_tuple_list])
   u_10m_mast2 = numpy.concatenate([getattr(x, 'u_10m_mast2') for x in data_tuple_list])
   phi_10m_mast2 = numpy.concatenate([getattr(x, 'phi_10m_mast2') for x in data_tuple_list])
   u_4m_mast2 = numpy.concatenate([getattr(x, 'u_4m_mast2') for x in data_tuple_list])

   n_times = times_utc.shape

   data_tuple = MetData(n_times = n_times,
                        times_utc = times_utc,
                        T_ambient = T_ambient,
                        u_10m_mast1 = u_10m_mast1,
                        phi_10m_mast1 = phi_10m_mast1,
                        u_10m_mast2 = u_10m_mast2,
                        phi_10m_mast2 = phi_10m_mast2,
                        u_4m_mast2 = u_4m_mast2)

   return data_tuple


def load(fpaths):

   data_tuple = None
   if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
      data_tuple = _load_single(fpaths)
   else:
      data_tuple = _concatenate(fpaths)

   return data_tuple

                                
                                
