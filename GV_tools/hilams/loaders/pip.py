#!/usr/bin/env python3


"""Structure and methods for Precipitation Imaging Package, Precipitation
Video Imager and Snow Video Imager data
"""

#--------------------------------------------------------------------------
# Structure and methods for Precipitation Imaging Package, Precipitation
# Video Imager and Snow Video Imager data
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import sys
import os
import collections
import numpy
import errno

import xlrd

from ...utilities import tools
from . import diagnostics

try:
   #Python3 version
   from configparser import ConfigParser
except ImportError:
   #Python2 version
   from ConfigParser import RawConfigParser as ConfigParser
   

#xlrd cell ctypes valid for conversion to float
_XL_CELL_NUMBER = 2

_MISSING_FLOAT = -999.
_MISSING_INTEGER = -999

_FOV_XDIM = None
_FOV_YDIM = None
_F_dof = None
_FPS = None
_DT_s = None
_Frame_sampling_interval = None
_DSD_sheet_name = None

PIPData = collections.namedtuple('PIPData', ['n_times', 
                                             'n_bins',
                                             'fov_x',
                                             'fov_y',
                                             'f_dof',
                                             'fps',
                                             'dt',
                                             'frame_sampling_interval',
                                             'times_utc', 
                                             'bin_lbounds', 
                                             'bin_widths', 
                                             'psd'])

def set_config(fpath_config=None):

    """Set parameters for camera and datafiles from config file fpath_config

       For info only, these were the settings for the original SVI (see Newman et al., 2009)
       _FOV_XDIM = 32.                    #mm
       _FOV_YDIM = 24.                    #mm
       _F_dof = 117.                      #unitless factor for calculating depth of field
       _FPS = 56.3                        #frames per second
       _DT_s = 60                         #Observation interval, seconds (saved as a numpy.deltatime64
                                          #object, so must be an integer.  Use integer seconds for now.
       _Frame_sampling_interval = 1       #The number of frames skipped when computing particle counts
                                          #from successive frames so that no particle is counted
                                          #multiple times.
       _DSD_sheet_name = 'DSD_min_L_lin'  #This was the sheet for the Feret diameter DSDs
    """

    global _FOV_XDIM, _FOV_YDIM, _F_dof, _FPS, _DT_s, _Frame_sampling_interval, _DSD_sheet_name

    if fpath_config is None:
        fpath_config = './pip.cfg'

    parser = ConfigParser()

    #The Python2 ConfigParser.RawConfigParser.read() and Python3 configparser.ConfigParser() methods
    #will silently ignore a missing config file and return an empty config list.

    config = parser.read(fpath_config)
    if len(config) > 0:

        _FOV_XDIM = parser.getfloat('Main','FOV_XDIM')
        _FOV_YDIM = parser.getfloat('Main', 'FOV_YDIM')
        _F_dof = parser.getfloat('Main', 'F_dof')
        _FPS = parser.getfloat('Main', 'FPS')
        _DT_s = parser.getint('Main', 'DT_s')
        _Frame_sampling_interval = parser.getint('Main', 'Frame_sampling_interval')
        _DSD_sheet_name = parser.get('Main', 'DSD_sheet_name')

    else:
        try:
            fp = open(fpath_config)
            fp.close()
        except IOError:
            sys.stderr.write('SVI/PVI/PIP configuration file not found at %s\n' %(fpath_config,))
            raise
    print('DSD_sheet_name', _DSD_sheet_name)
    return


def cell_list_to_numpy_float(cell_list):
    N_cells = len(cell_list)
    val_array = numpy.asarray([x.value if x.ctype in [_XL_CELL_NUMBER,] else _MISSING_FLOAT for x in cell_list], dtype=float)
    return val_array

def _load_single(fpath):

    data_tuple = None

    wb = xlrd.open_workbook(fpath, on_demand=True)

    #Get date information from Header sheet
    header_datasheet = wb.sheet_by_name('Header')
    datestring = header_datasheet.cell_value(3, 1)[3:11]
    year = int(datestring[0:4])
    month = int(datestring[4:6])
    day = int(datestring[6:8])

    psd_datasheet = wb.sheet_by_name(_DSD_sheet_name)

    N_rows = psd_datasheet.nrows
    N_cols = psd_datasheet.ncols
    N_bins = N_cols - 2
    N_samples = N_rows - 3

    #Instrument-specific settings (ideally, would come from the datafile)
    fov_x = _FOV_XDIM
    fov_y = _FOV_YDIM
    f_dof = _F_dof
    fps = _FPS
    dt = numpy.timedelta64(_DT_s, 's')


    #Calls to psd_datasheet.row return a list of xlrd Cells
    size_bins_lbound_row = psd_datasheet.row(0)
    size_bins_widths_row = psd_datasheet.row(1)

    size_bins_lbound = cell_list_to_numpy_float(size_bins_lbound_row[1:-1])
    size_bins_width = cell_list_to_numpy_float(size_bins_widths_row[1:-1])
    size_bins_ubound = size_bins_lbound + size_bins_width

    psd_array = numpy.empty((N_samples, N_bins), dtype=float)
    times_array = numpy.empty((N_samples,), dtype='datetime64[us]')

    for i_sample in range(N_samples):
        i_row = i_sample + 3

        row = psd_datasheet.row(i_row)
        #Process this row only if the first element can be converted
        #to a valid float.  Otherwise skip this row and continue
        #to the next
        if row[0].ctype in [_XL_CELL_NUMBER,]:        
            time_frac_day_utc = row[0].value
        else:
            continue
        hours, minutes, seconds = tools.frac_day_to_HMS(time_frac_day_utc)
        time_string = '%4d-%02d-%02dT%02d:%02d:%09.6f' %(year, month, day, hours, minutes, seconds)
        tmp = numpy.datetime64(time_string)
        times_array[i_sample] = numpy.datetime64(time_string)
        psd_vals = cell_list_to_numpy_float(row[1:-1])
        psd_array[i_sample,:] = psd_vals[:]

    data_tuple = PIPData(n_times = N_samples,
                         n_bins = N_bins,
                         fov_x = fov_x,
                         fov_y = fov_y,
                         f_dof = f_dof,
                         fps = fps,
                         dt = dt,
                         frame_sampling_interval = _Frame_sampling_interval,
                         times_utc = times_array,
                         bin_lbounds = size_bins_lbound,
                         bin_widths = size_bins_width,
                         psd = psd_array)

    wb.release_resources()
    del wb

    return data_tuple



def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        data_tuple_list.append(_load_single(fpath))

    fov_x_match = all([data_tuple_list[0].fov_x == getattr(x, 'fov_x') for x in data_tuple_list[1:]])
    fov_y_match = all([data_tuple_list[0].fov_y == getattr(x, 'fov_y') for x in data_tuple_list[1:]])
    f_dof_match = all([data_tuple_list[0].f_dof == getattr(x, 'f_dof') for x in data_tuple_list[1:]])
    fps_match = all([data_tuple_list[0].fps == getattr(x, 'fps') for x in data_tuple_list[1:]])
    dt_match = all([data_tuple_list[0].dt == getattr(x, 'dt') for x in data_tuple_list[1:]])
    frame_sampling_interval_match = all([data_tuple_list[0].frame_sampling_interval ==
                                    getattr(x, 'frame_sampling_interval') for x in data_tuple_list[1:]])
    lbounds_match = numpy.all([numpy.allclose(data_tuple_list[0].bin_lbounds,
                               getattr(x, 'bin_lbounds')) for x in data_tuple_list[1:]])
    widths_match = numpy.all([numpy.allclose(data_tuple_list[0].bin_widths,
                              getattr(x, 'bin_widths')) for x in data_tuple_list[1:]])
    if (lbounds_match and
        widths_match and
        fov_x_match and
        fov_y_match and
        f_dof_match and
        fps_match and
        dt_match and
        frame_sampling_interval_match):

        times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list])
        if numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
            psd_array = numpy.concatenate([getattr(x, 'psd') for x in data_tuple_list])

            n_times = times_utc.shape[0]
            n_bins = psd_array.shape[1]
        
            data_tuple = PIPData(n_bins = n_bins,
                                 n_times = n_times,
                                 fov_x = data_tuple_list[0].fov_x,
                                 fov_y = data_tuple_list[0].fov_y,
                                 f_dof = data_tuple_list[0].f_dof,
                                 fps = data_tuple_list[0].fps,
                                 dt = data_tuple_list[0].dt,
                                 frame_sampling_interval = data_tuple_list[0].frame_sampling_interval,
                                 bin_lbounds = data_tuple_list[0].bin_lbounds,
                                 bin_widths = data_tuple_list[0].bin_widths,
                                 times_utc = times_utc,
                                 psd = psd_array)
        else:
            sys.stderr.write('times_utc values not strictly increasing in loaders.pip._concatenate()\n')
            diagnostics.sequential_time_error_handler(data_tuple_list)
            assert 0
    else:
        sys.stderr.write('Inconsistencies in bin lbounds, widths, fov_x, fov_y, f_dof, fps, dt or frame_sampling_interval in loaders.pip._concatenate()\n')
        assert 0

    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)

    return data_tuple


def test():
    from ..dataproc import pip as pip_proc

    fpaths = ['./tests/tests_gcpex/data/GCPEx_pvi_20120124_huronia_103_dsd_dis2.xls',
              './tests/tests_gcpex/data/GCPEx_pvi_20120125_huronia_103_dsd_dis2_test.xls']

    set_config()
    data = load(fpaths)

    interval = numpy.timedelta64(300, 's')

    sample_start_times, Di_mm, Di_uncert, N_Di_mean, N_Di_uncert, N0_mm_fitted, lambda_mm_fitted = pip_proc.resample(data, t_interval=interval)
    N_intervals, N_bins  = N_Di_mean.shape

    Di_mm = data.bin_lbounds
    for i_interval in range(N_intervals):
        for i_size in range(N_bins):
            if (N_Di_mean[i_interval, i_size] > 0
                and N_Di_uncert[i_interval, i_size] > 0.):
                print('%15.5g %15.5g %15.5g %15.5g %15.5g' %(
                    Di_mm[i_size],
                    Di_uncert[i_interval, i_size],
                    N_Di_mean[i_interval, i_size],
                    N_Di_uncert[i_interval, i_size],
                    N_Di_uncert[i_interval, i_size]/N_Di_mean[i_interval, i_size]))


    #
if __name__ == '__main__':
    test()
