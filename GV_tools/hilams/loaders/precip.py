#!/usr/bin/env python3

"""Structure and methods for HiLaMS precipitationdata
"""

import sys
import numpy
import collections
import netCDF4

PrecipData = collections.namedtuple('PrecipData', ['n_times',
                                                   'times_utc',
                                                   'u_4m_DF',
                                                   'phi_4m_DF',
                                                   'accum_DF',
                                                   'T_rim_DF',
                                                   'accum_alter',
                                                   'T_rim_alter',
                                                   'accum_unshielded',
                                                   'T_rim_unshielded'])



def _load_single(fpath):

   dset = netCDF4.Dataset(fpath, 'r')
   ymd_var = dset.variables['YMD']
   seconds_var = dset.variables['sec']
   u_4m_DF_var = dset.variables['4m_wind_speed_DF']
   phi_4m_DF_var = dset.variables['4m_wind_dir_DF']
   accum_DF_var = dset.variables['double_fence_accum']
   accum_alter_var = dset.variables['single_south_accum']
   accum_unshielded_var = dset.variables['single_upstream_accum']
   T_rim_DF_var = dset.variables['double_fence_rimtemp']
   T_rim_alter_var = dset.variables['single_south_rimtemp']
   T_rim_unshielded_var = dset.variables['single_upstream_rimtemp']

   data_tuple = None
   N_times = ymd_var[:].shape[0]

   #Convert file times to datetime64 times_utc
   
   times_utc = numpy.zeros((N_times,), dtype='datetime64[s]')
   for i_time in range(N_times):
      ymd_str = '%s' %(ymd_var[i_time],)
      yr_str = ymd_str[0:4]
      mm_str = ymd_str[4:6]
      dd_str = ymd_str[6:8]

      base_time = numpy.datetime64('%s-%s-%sT00:00:00' %(yr_str, mm_str, dd_str))
      delta_time_s = numpy.timedelta64(int(seconds_var[i_time]), 's')
      times_utc[i_time] = base_time + delta_time_s

   data_tuple = PrecipData(n_times = N_times,
                           times_utc = times_utc,
                           u_4m_DF = u_4m_DF_var[:], 
                           phi_4m_DF = phi_4m_DF_var[:],
                           accum_DF = accum_DF_var[:],
                           accum_alter = accum_alter_var[:],
                           accum_unshielded = accum_unshielded_var[:],
                           T_rim_DF = T_rim_DF_var[:],
                           T_rim_alter = T_rim_alter_var[:],
                           T_rim_unshielded = T_rim_unshielded_var[:])


   dset.close()
   return data_tuple


def _concatenate(fpaths):

   data_tuple = None

   data_tuple_list_tmp = []
   for fpath in fpaths:
      data_tuple_list_tmp.append(_load_single(fpath))

   times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in data_tuple_list_tmp])

   #Make sure that time is strictly increasing
   if not numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's')):
      #Bail out
      sys.stderr.write('times_utc values not strictly increasing in loaders.mrr._concatenate()\n')
      diagnostics.sequential_time_error_handler(data_tuple_list)
      assert 0
   else:
      data_tuple_list = data_tuple_list_tmp
   
   #Create the concatenated data tuple
   u_4m_DF = numpy.concatenate([getattr(x, 'u_4m_DF') for x in data_tuple_list])
   phi_4m_DF = numpy.concatenate([getattr(x, 'phi_4m_DF') for x in data_tuple_list])
   accum_DF = numpy.concatenate([getattr(x, 'accum_DF') for x in data_tuple_list], axis=0)
   accum_alter = numpy.concatenate([getattr(x, 'accum_alter') for x in data_tuple_list], axis=0)
   accum_unshielded = numpy.concatenate([getattr(x, 'accum_unshielded') for x in data_tuple_list], axis=0)
   T_rim_DF = numpy.concatenate([getattr(x, 'T_rim_DF') for x in data_tuple_list])
   T_rim_alter = numpy.concatenate([getattr(x, 'T_rim_alter') for x in data_tuple_list])
   T_rim_unshielded = numpy.concatenate([getattr(x, 'T_rim_alter') for x in data_tuple_list])

   n_times = times_utc.shape

   data_tuple = PrecipData(n_times = n_times,
                           times_utc = times_utc,
                           u_4m_DF = u_4m_DF,
                           phi_4m_DF = phi_4m_DF,
                           accum_DF = accum_DF,
                           accum_alter = accum_alter,
                           accum_unshielded = accum_unshielded,
                           T_rim_DF = T_rim_DF,
                           T_rim_alter = T_rim_alter,
                           T_rim_unshielded = T_rim_unshielded)

   return data_tuple


def load(fpaths):

   data_tuple = None
   if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
      data_tuple = _load_single(fpaths)
   else:
      data_tuple = _concatenate(fpaths)

   return data_tuple

