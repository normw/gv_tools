#!/usr/bin/env python3

"""Description
"""

#--------------------------------------------------------------------------
# LPVEx event definitions
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


"""LPVEx event definitions

Copyright (c) 2020 Norman Wood.  All rights reserved.

This file is part of GV_tools.  It is subject to the license terms of
the BSD-3-Clause license, the full text of which is available in the
LICENSE file found in the top-level directory of the GV_tools
distribution.  No part of GV_tools, including this file, may be copied,
modified, propagated, or distributed except according to the terms
contained in the LICENSE file.
"""


import numpy

event_dict = {'E02':('2012-01-19T15:00:00', '2012-01-20T04:00:00'),
              'E03':('2012-01-21T06:00:00', '2012-01-21T23:00:00'),
              'E05':('2012-01-24T04:00:00', '2012-01-25T03:00:00'),
              'E07':('2012-01-28T13:00:00', '2012-01-29T12:00:00'),
              'E08':('2012-01-30T20:00:00', '2012-01-31T04:00:00'),
              'E11':('2012-02-07T02:00:00', '2012-02-07T12:00:00'),
              'E12':('2012-02-10T19:00:00', '2012-02-11T12:00:00'),
              'E13':('2012-02-11T21:00:00', '2012-02-12T14:00:00'),
              'E14':('2012-02-12T16:00:00', '2012-02-13T02:00:00'),
              'E15':('2012-02-14T08:00:00', '2012-02-15T14:00:00'),
              'E17':('2012-02-18T10:00:00', '2012-02-18T20:00:00'),
              'E19':('2012-02-21T18:00:00', '2012-02-22T07:00:00'),
              'E20':('2012-02-24T11:00:00', '2012-02-25T00:00:00'),
              'E21':('2012-02-25T01:00:00', '2012-02-25T17:00:00'),
              'E22':('2012-02-27T20:00:00', '2012-02-28T10:00:00'),
              'E23':('2012-02-29T12:00:00', '2012-03-01T10:00:00'),
              'E25':('2012-03-04T00:00:00', '2012-03-04T13:00:00')}


def test():
    for event in event_dict.keys():
        t_start = numpy.datetime64(event_dict[event][0])
        t_end = numpy.datetime64(event_dict[event][1])
        print(t_start, t_end)

if __name__ == "__main__":
    test()
