#!/usr/bin/env python

#Example script for running a test on a module from a command-line script
#To handle the relative imports in the package, need to insert the location
#into sys.path the path to the development directory

import sys

sys.path.insert(0, '/csudisk/Devel')
import GV_tools.gcpex.loaders.mrr

fpaths = ['/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/MRR/Huronia/NetCDF/MRR_20120128_GCPEx_HURONIA-EC_snow.nc',
          '/boltzmann/data6/norm/GV_precip/GCPEx/Observation_data/Surface/MRR/Huronia/NetCDF/MRR_20120129_GCPEx_HURONIA-EC_snow.nc']

data_list = []
data_list.append(GV_tools.gcpex.loaders.mrr._load_single(fpaths[0]))
data_list.append(GV_tools.gcpex.loaders.mrr._load_single(fpaths[1]))
assert len(data_list) == 2
for x in data_list:
    print x.times_utc.shape
    print x.times_utc[0], x.times_utc[-1]


_new_list = GV_tools.gcpex.loaders.mrr._clean_overlaps(data_list)

assert len(_new_list) == 2
for x in _new_list:
    print x.times_utc.shape
    print x.times_utc[0], x.times_utc[-1]
print "Cleaning OK"
